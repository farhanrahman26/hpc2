#include "simpsons.hpp"
#include <math.h>
#include <stdio.h>
#include <iostream>
#include <cstdlib>
/*=====================Test code for simpsons==================*/



void TestSimpsonF0(void){
	double exact=(exp(1)-1);	// Exact result
	float a[1]={0};
	float b[1]={1};

	for (unsigned n = 2; n < 2048; n*=2){
		double res = Integrator1DSimpsons(&a[0],&b[0],n);
		printf("TestSimpsonF0, n=%d, value=%lf, error=%lg\n", n, res, res-exact);
	}
}

double F1Simp(float x, float y, float *params){
	return sin(params[0] + x*y) * exp(params[1]+x);
}




void TestSimpsonF1(void){
	double exact=1.95683793560212f;	// Correct to about 10 digits
	float a[2]={0,0};
	float b[2]={1,1};
	float params[2]={0.5,0.5};
	int n;

	for(n=2;n<=1024;n*=2){		
		double res = Integrator2DSimpsons(a,b,n,params);
		printf("F1, n=%d, value=%lf, error=%lg\n", n, res, res-exact);
	}
}


/*==============3D functions===================*/

/* Name = (round(exp(-x))-round(exp(y))*sin(z)
	Code = 2
	k=3 (tri-variate)
	params = []  (no parameters)
*/
float F2_S(const float x, const float y, const float z, const float *params)
{
	return round(exp(-x))-round(exp(y))*sin(z);
}

/* Name = Three-dimensional sinc
	Code = 3
	k=3 (tri-variate)
	params = [norm_lev]  (1 parameter)
*/
float F3_S(const float x, const float y, const float z, const float *params)
{
	// Perform a vector norm of a specific power, e.g. if params[0]==2 then it is the euclidian norm
	float v=pow(x,params[0]) + pow(y,params[0]) + pow(z,params[0]);
	v=pow(v,1/params[0]);
	// Then a sinc
	return v == 0.0 ? 1 : sin(v)/v;
}

/* Name = Gaussian CDF over 3 dimensions
	Code = 4
	k=3 (tri-variate)
	params= [sigma_0..sigma_8, scale]  (10 parameters)

	Sigma is the covariance matrix, but here we need the
	inverse of sigma. It's not worth calculating over and
	over, so we pre-calculate the inverse and store it in
	params. Don't worry too much about the details of the
	matrix, it isn't relative to making it go fast.
*/
float F4_S(const float x, const float y, const float z, const float *params)
{
	// We need to calculate scale*exp(-x'*\Sigma^{-1}*x/2),
	// where \Sigma^{-1} is a 3x3 matrix.
	int i;
	float acc=0;
	float _x[3] = {x,y,z};
	for(i=0;i<3;i++){
		// We first form a row of the \Sigma^-1 * x
		float row= params[i*3+0]*x + params[i*3+1]*y + params[i*3+2]*z;
		// Then perform the left mutiply with x'
		acc += row*_x[i];
	}
	return params[9] * exp(-acc / 2);
}

/* Name = Powerful Pony
	Code = 5
	k=3 (tri-variate)
	params= [] (no parameters)
*/
float F5_S(const float x, const float y, const float z, const float *params)
{
	return powf(powf(sinf( pow(x, y) ), 2), z);
}

/* Name = Sphere Shell
	Code = 6
	k=3 (tri-variate)
	params= [radius,width]
*/
float F6_S(const float x, const float y, const float z, const float *params)
{
	float a=powf(powf(x,2)+powf(y,2)+powf(z,2),0.5); // distance from origin
	float d=(a-params[0]);	// How far from surface of sphere
	if(d<0)
		d=-d;
	if(d<params[1]){
		return 1;
	}else{
		return 0;
	}
}

/*==============3D functions===================*/



void TestSimpsonF2()
{
	double exact=9.48557252267795;	// Correct to about 6 digits
	float a[3]={-1,-1,-1};
	float b[3]={1,1,1};
	int n;
	
	for(n=2;n<=256;n*=2){		
		double res=Integrator3DSimpsons(a, b, n, NULL, 2);
		printf("F2, n=%d, value=%lf, error=%lg\n", n, res, res-exact);
	}
}

void TestSimpsonF3()
{
	double exact=-7.18387139942142f;	// Correct to about 6 digits
	float a[3]={0,0,0};
	float b[3]={5,5,5};
	float params[1]={2};
	int n;
	
	for(n=2;n<=256;n*=2){			
		double res=Integrator3DSimpsons(a, b, n, params, 3);
		printf("F3, n=%d, value=%lf, error=%lg\n", n, res, res-exact);
	}
}

void TestSimpsonF4()
{
	double exact=0.677779532970409f;	// Correct to about 8 digits
	float a[3]={-16,-16,-16};	// We're going to cheat, and assume -16=-infinity.
	float b[3]={1,1,1};
	// We're going to use the covariance matrix with ones on the diagonal, and
	// 0.5 off the diagonal.
	const float PI=3.1415926535897932384626433832795f;
	float params[10]={
		1.5, -0.5, -0.5,
		-0.5, 1.5, -0.5,
		-0.5, -0.5, 1.5,
		pow(2*PI,-3.0/2.0)*pow(0.5,-0.5) // This is the scale factor
	};
	int n;
	
	for(n=2;n<=512;n*=2){		
		double res= Integrator3DSimpsons(a, b, n, params, 4);
		printf("F4, n=%d, value=%lf, error=%lg	\n", n, res, res-exact);
	}
}

void TestSimpsonF5()
{
	double exact=13.4249394627056;	// Correct to about 6 digits
	float a[3]={0,0,0};
	float b[3]={3,3,3};
	int n;
	
	for(n=2;n<=512;n*=2){			
		double res=Integrator3DSimpsons(a, b, n, NULL, 5);
		printf("F5, n=%d, value=%lf, error=%lg	\n", n, res, res-exact);
	}
}

void TestSimpsonF6()
{
	// Integrate over a shell with radius 3 and width 0.02
	//  = volume of a sphere of 3.01 minus a sphere of 2.99
	double exact=   2.261955088165;
	float a[3]={-4,-4,-4};
	float b[3]={4,4,4};
	float params[2]={3,0.01};
	int n;
	
	for(n=2;n<=2048;n*=2){
		double res=Integrator3DSimpsons(a, b, n, params, 6);
		printf("F6, n=%d, value=%lf, error=%lg	\n", n, res, res-exact);
	}
}



/*Integrators*/
double Integrator1DSimpsons(float *a, float *b, int n){
	float acc = 0.0f;
	float xn, xnp1;
	float stride = (b[0]-a[0])/(n*1.0f);
	float mid = 0.0f;
	for (unsigned i = 0; i < n; ++i){
		xn = a[0] + ((float) i)*stride;
		xnp1 = xn+stride;
		mid = (xn+xnp1)/2.0f;
		acc += (exp(xn) + 4*exp(mid) + exp(xnp1));
	}
	return acc*((b[0]-a[0])/(n*6.0f));
}

double Integrator2DSimpsons(float *a, float *b, int n, float *params){
	double acc = 0.0;
	double xn, xnp1;
	double yn, ynp1;
	double xstride = (b[0]-a[0])/(n*1.0);
	double ystride = (b[1]-a[1])/(n*1.0);
	double midX = 0.0;
	double midY = 0.0;

	for(unsigned i = 0; i < n; ++i){
			yn = a[1] + ((double) i)*ystride;		
			ynp1 = yn+ystride;
			midY = (yn+ynp1)/2.0;
			for (unsigned j = 0; j < n; ++j){
				xn = a[0] + ((double) j)*xstride;
				xnp1 = xn+xstride;
				midX = (xn+xnp1)/2.0;
				acc += F1Simp(xn,ynp1,params) + 4.0*F1Simp(midX,ynp1,params) + F1Simp(xnp1,ynp1,params); 
				acc += 4.0*F1Simp(xn,midY,params) + 16.0*F1Simp(midX,midY,params) + 4.0*F1Simp(xnp1,midY,params);
				acc += F1Simp(xn,yn,params) + 4.0*F1Simp(midX,yn,params) + F1Simp(xnp1,yn,params);
			}
	}

	return acc*((b[0]-a[0]))*((b[1]-a[1]))/(n*n*36.0);
}


double Integrator3DSimpsons(float *a, float *b, int n, float *params, int functionCode){
	double acc = 0.0;
	double xn, xnp1;
	double yn, ynp1;
	double zn, znp1;
	double xstride = (b[0]-a[0])/(n*1.0);
	double ystride = (b[1]-a[1])/(n*1.0);
	double zstride = (b[2]-a[2])/(n*1.0);
	double midX = 0.0;
	double midY = 0.0;
	double midZ = 0.0;
	for(unsigned i = 0; i < n; ++i){
		zn = a[2] + ((double) i)*zstride;
		znp1 = zn + zstride;
		midZ = (zn+znp1)/2.0;
		for(unsigned j = 0; j < n; ++j){
				yn = a[1] + ((double) j)*ystride;		
				ynp1 = yn+ystride;
				midY = (yn+ynp1)/2.0;
			for (unsigned k = 0; k < n; ++k){
				xn = a[0] + ((double) k)*xstride;
				xnp1 = xn+xstride;
				midX = (xn+xnp1)/2.0;
				switch(functionCode){
					case 2:
						acc += 1.0*F2_S(xn,ynp1,zn,params) + 4.0*F2_S(midX,ynp1,zn,params) + 1.0*F2_S(xnp1,ynp1,zn,params);					
						acc += 4.0*F2_S(xn,midY,zn,params) + 16.0*F2_S(midX,midY,zn,params) + 4.0*F2_S(xnp1,midY,zn,params);
						acc += 1.0*F2_S(xn,yn,zn,params) + 4.0*F2_S(midX,yn,zn,params) + 1.0*F2_S(xnp1,yn,zn,params);

						acc += 4.0*F2_S(xn,ynp1,midZ,params) + 16.0*F2_S(midX,ynp1,midZ,params) + 4.0*F2_S(xnp1,ynp1,midZ,params);					
						acc += 16.0*F2_S(xn,midY,midZ,params) + 64.0*F2_S(midX,midY,midZ,params) + 16.0*F2_S(xnp1,midY,midZ,params);
						acc += 4.0*F2_S(xn,yn,midZ,params) + 16.0*F2_S(midX,yn,midZ,params) + 4.0*F2_S(xnp1,yn,midZ,params);

						acc += 1.0*F2_S(xn,ynp1,znp1,params) + 4.0*F2_S(midX,ynp1,znp1,params) + 1.0*F2_S(xnp1,ynp1,znp1,params);					
						acc += 4.0*F2_S(xn,midY,znp1,params) + 16.0*F2_S(midX,midY,znp1,params) + 4.0*F2_S(xnp1,midY,znp1,params);
						acc += 1.0*F2_S(xn,yn,znp1,params) + 4.0*F2_S(midX,yn,znp1,params) + 1.0*F2_S(xnp1,yn,znp1,params);

						break;
					case 3:
						acc += 1.0*F3_S(xn,ynp1,zn,params) + 4.0*F3_S(midX,ynp1,zn,params) + 1.0*F3_S(xnp1,ynp1,zn,params);					
						acc += 4.0*F3_S(xn,midY,zn,params) + 16.0*F3_S(midX,midY,zn,params) + 4.0*F3_S(xnp1,midY,zn,params);
						acc += 1.0*F3_S(xn,yn,zn,params) + 4.0*F3_S(midX,yn,zn,params) + 1.0*F3_S(xnp1,yn,zn,params);

						acc += 4.0*F3_S(xn,ynp1,midZ,params) + 16.0*F3_S(midX,ynp1,midZ,params) + 4.0*F3_S(xnp1,ynp1,midZ,params);					
						acc += 16.0*F3_S(xn,midY,midZ,params) + 64.0*F3_S(midX,midY,midZ,params) + 16.0*F3_S(xnp1,midY,midZ,params);
						acc += 4.0*F3_S(xn,yn,midZ,params) + 16.0*F3_S(midX,yn,midZ,params) + 4.0*F3_S(xnp1,yn,midZ,params);

						acc += 1.0*F3_S(xn,ynp1,znp1,params) + 4.0*F3_S(midX,ynp1,znp1,params) + 1.0*F3_S(xnp1,ynp1,znp1,params);					
						acc += 4.0*F3_S(xn,midY,znp1,params) + 16.0*F3_S(midX,midY,znp1,params) + 4.0*F3_S(xnp1,midY,znp1,params);
						acc += 1.0*F3_S(xn,yn,znp1,params) + 4.0*F3_S(midX,yn,znp1,params) + 1.0*F3_S(xnp1,yn,znp1,params);


						break;
					case 4:
						acc += 1.0*F4_S(xn,ynp1,zn,params) + 4.0*F4_S(midX,ynp1,zn,params) + 1.0*F4_S(xnp1,ynp1,zn,params);					
						acc += 4.0*F4_S(xn,midY,zn,params) + 16.0*F4_S(midX,midY,zn,params) + 4.0*F4_S(xnp1,midY,zn,params);
						acc += 1.0*F4_S(xn,yn,zn,params) + 4.0*F4_S(midX,yn,zn,params) + 1.0*F4_S(xnp1,yn,zn,params);

						acc += 4.0*F4_S(xn,ynp1,midZ,params) + 16.0*F4_S(midX,ynp1,midZ,params) + 4.0*F4_S(xnp1,ynp1,midZ,params);					
						acc += 16.0*F4_S(xn,midY,midZ,params) + 64.0*F4_S(midX,midY,midZ,params) + 16.0*F4_S(xnp1,midY,midZ,params);
						acc += 4.0*F4_S(xn,yn,midZ,params) + 16.0*F4_S(midX,yn,midZ,params) + 4.0*F4_S(xnp1,yn,midZ,params);

						acc += 1.0*F4_S(xn,ynp1,znp1,params) + 4.0*F4_S(midX,ynp1,znp1,params) + 1.0*F4_S(xnp1,ynp1,znp1,params);					
						acc += 4.0*F4_S(xn,midY,znp1,params) + 16.0*F4_S(midX,midY,znp1,params) + 4.0*F4_S(xnp1,midY,znp1,params);
						acc += 1.0*F4_S(xn,yn,znp1,params) + 4.0*F4_S(midX,yn,znp1,params) + 1.0*F4_S(xnp1,yn,znp1,params);

						break;
					case 5:
						acc += 1.0*F5_S(xn,ynp1,zn,params) + 4.0*F5_S(midX,ynp1,zn,params) + 1.0*F5_S(xnp1,ynp1,zn,params);					
						acc += 4.0*F5_S(xn,midY,zn,params) + 16.0*F5_S(midX,midY,zn,params) + 4.0*F5_S(xnp1,midY,zn,params);
						acc += 1.0*F5_S(xn,yn,zn,params) + 4.0*F5_S(midX,yn,zn,params) + 1.0*F5_S(xnp1,yn,zn,params);

						acc += 4.0*F5_S(xn,ynp1,midZ,params) + 16.0*F5_S(midX,ynp1,midZ,params) + 4.0*F5_S(xnp1,ynp1,midZ,params);					
						acc += 16.0*F5_S(xn,midY,midZ,params) + 64.0*F5_S(midX,midY,midZ,params) + 16.0*F5_S(xnp1,midY,midZ,params);
						acc += 4.0*F5_S(xn,yn,midZ,params) + 16.0*F5_S(midX,yn,midZ,params) + 4.0*F5_S(xnp1,yn,midZ,params);

						acc += 1.0*F5_S(xn,ynp1,znp1,params) + 4.0*F5_S(midX,ynp1,znp1,params) + 1.0*F5_S(xnp1,ynp1,znp1,params);					
						acc += 4.0*F5_S(xn,midY,znp1,params) + 16.0*F5_S(midX,midY,znp1,params) + 4.0*F5_S(xnp1,midY,znp1,params);
						acc += 1.0*F5_S(xn,yn,znp1,params) + 4.0*F5_S(midX,yn,znp1,params) + 1.0*F5_S(xnp1,yn,znp1,params);

						break;
					case 6:
						acc += 1.0*F6_S(xn,ynp1,zn,params) + 4.0*F6_S(midX,ynp1,zn,params) + 1.0*F6_S(xnp1,ynp1,zn,params);					
						acc += 4.0*F6_S(xn,midY,zn,params) + 16.0*F6_S(midX,midY,zn,params) + 4.0*F6_S(xnp1,midY,zn,params);
						acc += 1.0*F6_S(xn,yn,zn,params) + 4.0*F6_S(midX,yn,zn,params) + 1.0*F6_S(xnp1,yn,zn,params);

						acc += 4.0*F6_S(xn,ynp1,midZ,params) + 16.0*F6_S(midX,ynp1,midZ,params) + 4.0*F6_S(xnp1,ynp1,midZ,params);					
						acc += 16.0*F6_S(xn,midY,midZ,params) + 64.0*F6_S(midX,midY,midZ,params) + 16.0*F6_S(xnp1,midY,midZ,params);
						acc += 4.0*F6_S(xn,yn,midZ,params) + 16.0*F6_S(midX,yn,midZ,params) + 4.0*F6_S(xnp1,yn,midZ,params);

						acc += 1.0*F6_S(xn,ynp1,znp1,params) + 4.0*F6_S(midX,ynp1,znp1,params) + 1.0*F6_S(xnp1,ynp1,znp1,params);					
						acc += 4.0*F6_S(xn,midY,znp1,params) + 16.0*F6_S(midX,midY,znp1,params) + 4.0*F6_S(xnp1,midY,znp1,params);
						acc += 1.0*F6_S(xn,yn,znp1,params) + 4.0*F6_S(midX,yn,znp1,params) + 1.0*F6_S(xnp1,yn,znp1,params);

						break;
					default:
						std::cerr << "Function not recognised" << std::endl;
						exit(1);
						break;
				}
			}
		}
	}
	return acc*((b[0]-a[0]))*((b[1]-a[1]))*((b[2]-a[2]))/(n*n*n*216.0);

}


