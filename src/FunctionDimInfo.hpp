#ifndef FUNCTION_DIM_INFO_HPP
#define FUNCTION_DIM_INFO_HPP
#include <iostream>

/*Class containing information about
 *the  dimensionality of the test functions*/
class FunctionDimInfo{
public:
	static const int F0;
	static const int F1;
	static const int F2;
	static const int F3;
	static const int F4;
	static const int F5;
	static const int F6;

	static int dimension(int functionCode) {
		int k = -1;
		switch(functionCode){
			case 0:	k=FunctionDimInfo::F0 ;	break;
			case 1:	k=FunctionDimInfo::F1 ;	break;
			case 2:	k=FunctionDimInfo::F2 ;	break;
			case 3:	k=FunctionDimInfo::F3 ;	break;
			case 4:	k=FunctionDimInfo::F4 ;	break;
			case 5:	k=FunctionDimInfo::F5 ;	break;
			case 6:	k=FunctionDimInfo::F6 ;	break;
			default:
				std::cerr << "Invalid function code." << std::endl;
				exit(1);
		}
		return k;
	}

};

const int FunctionDimInfo::F0 = 1;
const int FunctionDimInfo::F1 = 2;
const int FunctionDimInfo::F2 = 3;
const int FunctionDimInfo::F3 = 3;
const int FunctionDimInfo::F4 = 3;
const int FunctionDimInfo::F5 = 3;
const int FunctionDimInfo::F6 = 3;


#endif
