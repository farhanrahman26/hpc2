#ifndef KERNEL_F4_HPP
#define KERNEL_F4_HPP
#include "Kernel.hpp"
#if defined(__APPLE__) || defined(MACOSX)
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif
#include "CLError.hpp"

#define MAX_F4_BUFFER 512*512*512

class KernelF4 : public Kernel{
public:
       KernelF4(
                cl_program clProgram,
                cl_command_queue commandQueue,
                cl_context ctx,
                const float* a,
                const float* b,
                const float* params,
                int n0,
                int n1,
                int n2,
                std::string kernelName = "F4_GPU",
				int reductionFactor = 1
        ) : Kernel(clProgram, commandQueue, ctx, kernelName, 
				a, b, 3, params, 10, n0, n1, n2, reductionFactor, MAX_F4_BUFFER
			) 
        {
        }

        ~KernelF4(void){
                clReleaseMemObject(d_y);
				clReleaseMemObject(d_params);
        }
};

#endif
