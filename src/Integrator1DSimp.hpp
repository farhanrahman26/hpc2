#ifndef INTEGRATOR_1D_SIMP_HPP
#define INTEGRATOR_1D_SIMP_HPP

#include "Integrator.hpp"
#include "InvalidDimException.hpp"
#include "Kernel.hpp"
#include "CLError.hpp"

class Integrator1DSimp : public Integrator{
public:
	Integrator1DSimp(
		int functionCode,
		const float *a,
		const float *b,
		float eps,
		const float *params,
		float *errorEstimate 
	) throw(InvalidDimException) : Integrator(functionCode, a, b, eps, params, errorEstimate){
		
		if (functionCode != 0){
			/*Throw exception if dimensionality of integration does not match*/
			throw InvalidDimException(functionCode, 1);
		}
	}


	/*Integration 1 dimensional*/
	double integrate(int n, Kernel &kernel){
		unsigned n0 = n;
		float acc = 0.0f;
		float *x = new float[1];
		float *y = new float[n0];

//		const unsigned threadsPerBlock = 4;
		const int targetReduction = 8;
		const int reductionFactor = n < targetReduction ? 1 : targetReduction ;
		
//		size_t localws[1] = {n0 < threadsPerBlock ? 1 : threadsPerBlock};
		size_t globalws[1] = {n0/reductionFactor};
		std::vector<float *> hostData;
		hostData.push_back(y);

		kernel.setReductionFactor(reductionFactor);

		try{
			kernel.launch(globalws, NULL, 1, n0/reductionFactor);
		} catch(CLError e){
			std::cerr << e.what() << std::endl;
			exit(1);
		}

		kernel.getData(hostData, n0/reductionFactor);
	
		for(unsigned i0 = 0; i0 < n0/reductionFactor; ++i0){
			acc += y[i0];									
		}

		delete y;
		delete x;

		return acc*((b[0]-a[0])/(n*6.0f));	
	}	

};


#endif