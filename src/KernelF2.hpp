#ifndef KERNEL_F2_HPP
#define KERNEL_F2_HPP
#include "Kernel.hpp"
#if defined(__APPLE__) || defined(MACOSX)
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif
#include "CLError.hpp"

#define MAX_F2_BUFFER 256*256*256

class KernelF2 : public Kernel{
public:
	KernelF2(
		cl_program clProgram,
		cl_command_queue commandQueue,
		cl_context ctx,
		const float* a,
		const float* b,
		int n0,
		int n1,
		int n2,
		std::string kernelName = "F2_GPU",
		int reductionFactor = 1
	) : Kernel(
			clProgram, 
			commandQueue, 
			ctx, 
			kernelName, 
			a, b, 3, NULL, 0, n0, n1, n2, reductionFactor, MAX_F2_BUFFER
		)
	{			

	}

	~KernelF2(void){
		clReleaseMemObject(d_y);
	}
};

#endif
