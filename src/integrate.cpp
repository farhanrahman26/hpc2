#include "integrate.hpp"
#include "headers.hpp"
#include <limits>
#include <iostream>
#include <stdio.h>
#include <cstdlib>

double Integrate(
	int functionCode, // Identifies the function (and dimensionality k)
  	const float *a, // An array of k lower bounds
  	const float *b, // An array of k upper bounds
  	float eps,  // A target accuracy
  	const float *params, // Parameters to function
  	float *errorEstimate // Estimated error in integral
){

	const unsigned limits[] = {2,4,8,16,32,64,128,512,1024};


	const unsigned size = 8;
	const unsigned sizeThreeD = 7;

	Kernel *kernel = NULL;
	Integrator *integrator = NULL;
	
	std::vector<std::string> programSources;
	programSources.push_back("functions.cl");
	CLInitialiser cli(programSources);

	switch(functionCode){
		case 0:
			kernel = new KernelF0(
							cli.clProgram, 
							cli.commandQueue, 
							cli.ctx, 
							&a[0], 
							&b[0], 
							512,
							"F0_GPU_SIMP"
						);

			integrator = new Integrator1DSimp(
							0,
							&a[0],
							&b[0],
							eps,
							params,
							errorEstimate
						);		
			break;
		case 1:
			kernel = new KernelF1(
							cli.clProgram, 
							cli.commandQueue, 
							cli.ctx, 
							&a[0], 
							&b[0], 
							&params[0], 
							512, 
							512,
							"F1_GPU_SIMP"
						);

			integrator = new Integrator2DSimp(
							1, 
							&a[0], 
							&b[0], 
							eps,
							&params[0],
							errorEstimate
						);		
			break;
		case 2:
			kernel = new KernelF2(
							cli.clProgram, 
							cli.commandQueue, 
							cli.ctx, 
							&a[0], 
							&b[0], 
							512, 
							512, 
							512,
							"F2_GPU_SIMP"
						);

			integrator = new Integrator3DSimp(
							2,
							&a[0],
							&b[0],
							eps,
							&params[0],
							errorEstimate
						);		
			break;
		case 3:
			kernel = new KernelF3(
							cli.clProgram, 
							cli.commandQueue, 
							cli.ctx, 
							&a[0], 
							&b[0],
							&params[0], 
							512, 
							512, 
							512,
							"F3_GPU_SIMP"
						);

			integrator = new Integrator3DSimp(
							3,
							&a[0],
							&b[0],
							eps,
							&params[0],
							errorEstimate
						);

			break;
		case 4:
			kernel = new KernelF4(
							cli.clProgram, 
							cli.commandQueue, 
							cli.ctx, 
							&a[0], 
							&b[0], 
							&params[0], 
							512, 
							512,
							512,
							"F4_GPU_SIMP"
						);

			integrator = new Integrator3DSimp(
							4, 
							&a[0], 
							&b[0], 
							eps,
							&params[0],
							errorEstimate
						);		
			break;
		case 5:
			kernel = new KernelF5(
							cli.clProgram, 
							cli.commandQueue, 
							cli.ctx, 
							&a[0], 
							&b[0], 
							512, 
							512,
							512
						);

			integrator = new Integrator3D(
							5, 
							&a[0], 
							&b[0], 
							eps,
							&params[0],
							errorEstimate
						);		
			break;
		case 6:
			kernel = new KernelF6(
							cli.clProgram, 
							cli.commandQueue, 
							cli.ctx, 
							&a[0], 
							&b[0], 
							&params[0], 
							512, 
							512,
							512,
							"F6_GPU_SIMP"
						);

			integrator = new Integrator3DSimp(
							6, 
							&a[0], 
							&b[0], 
							eps,
							&params[0],
							errorEstimate
						);		
			break;
		default:
			std::cerr << "Function code not recognised" << std::endl;
			exit(1);
			break;
	}

	/*Get the max error*/
	float error = std::numeric_limits<float>::max();
	double ret = 0.0f;
	double prev = 0.0f;
	unsigned index = 1;
	while(fabs(error) > fabs(eps)){
		kernel->setN(limits[index]);
		ret = integrator->integrate(limits[index], *kernel);
		kernel->setN(limits[index-1]);
		prev = integrator->integrate(limits[index-1],*kernel);

		error = fabs(ret) - fabs(prev);
		if((functionCode == 0) || (functionCode == 1)){
			if(++index >= size)
				break;
		} else {
			if(++index >= sizeThreeD)
				break;
		}
	}
	delete integrator;
	delete kernel;
	(*errorEstimate) = error;

	return ret;
}
