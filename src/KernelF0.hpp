#ifndef KERNEL_F0_HPP
#define KERNEL_F0_HPP
#include "Kernel.hpp"
#include "CLError.hpp"
#if defined(__APPLE__) || defined(MACOSX)
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif

#define MAX_F0_BUFFER 512

class KernelF0 : public Kernel {
public:
	/*@param[a] lower bound of integration
	 *@param[b] upper bound of integration*/
	KernelF0(
		cl_program clProgram,
		cl_command_queue commandQueue,
		cl_context ctx,
		const float* a,
		const float* b,
		int n0,
		std::string kernelName = "F0_GPU",
		int reductionFactor = 1
	) : Kernel(clProgram, commandQueue, ctx, kernelName, 
			a, b, 1, NULL, 0, n0, 0, 0, reductionFactor, MAX_F0_BUFFER){
	}

	~KernelF0(void) {
		clReleaseMemObject(d_y);
	}
};


#endif
