#ifndef INVALID_DIM_EXCEPTION_HPP
#define INVALID_DIM_EXCEPTION_HPP

#include <iostream>
#include <string>
#include <sstream>

template<typename T>
std::string toString(T t){
	std::stringstream ss;
	std::string str;
	ss << t;
	ss >> str;
	return str;
}

class InvalidDimException : public std::exception{
public:
	/**Constructor for InvalidDimException
	 * @param[functionDim] dimension of function
	 * @param[integratorDim] the dimension of the integrator*/
	InvalidDimException(
		int functionDim,
		int integratorDim
	) : message("Expected dimension " 
				+ toString(integratorDim) 
				+ ", but got " 
				+ toString(functionDim)) {} 

	~InvalidDimException() throw() {}

	const char* what() const throw(){
		const std::string ret = this->message;
		return ret.c_str();
	} 

    std::string getMessage(void){
       	return this->message;
    }   

protected:
	std::string message;
};


#endif
