#ifndef KERNEL_F1_HPP
#define KERNEL_F1_HPP
#include "Kernel.hpp"
#include <string>

#define MAX_F1_BUFFER 1024*1024

class KernelF1 : public Kernel{
public:
	KernelF1(
		cl_program clProgram,
		cl_command_queue commandQueue,
		cl_context ctx,
		const float* a,
		const float* b,
		const float* params,
		int n0,
		int n1,
		std::string kernelName = "F1_GPU",
		int reductionFactor = 1
	) : Kernel(clProgram, commandQueue, ctx, kernelName, 
			a, b, 2, params, 2, n0, n1, 0, reductionFactor, MAX_F1_BUFFER) {

	}

	~KernelF1(void) {
		clReleaseMemObject(d_y);
		clReleaseMemObject(d_params);
	}
};

#endif
