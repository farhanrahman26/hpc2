#ifndef KERNEL_F3_HPP
#define KERNEL_F3_HPP
#include "Kernel.hpp"
#if defined(__APPLE__) || defined(MACOSX)
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif
#include "CLError.hpp"

#define MAX_F3_BUFFER 256*256*256

class KernelF3 : public Kernel{
public:
	KernelF3(
		cl_program clProgram,
		cl_command_queue commandQueue,
		cl_context ctx,
		const float* a,
		const float* b,
		const float *params,
		int n0,
		int n1,
		int n2,
		std::string kernelName = "F3_GPU",
		int reductionFactor = 1
	) : Kernel(clProgram, commandQueue, ctx, kernelName, 
		a, b, 3, params, 1, n0, n1, n2, reductionFactor, MAX_F3_BUFFER
		) 
	{

	}

	~KernelF3(void){
		clReleaseMemObject(d_y);
		clReleaseMemObject(d_params);
	}
};

#endif
