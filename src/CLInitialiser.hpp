#ifndef CL_INITIALISER_HPP
#define CL_INITIALISER_HPP
#if defined(__APPLE__) || defined(MACOSX)
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include "CLError.hpp"

#define ERRCHK(ciErrNum , message) if (ciErrNum != CL_SUCCESS) {std::cerr<< message << std::endl; throw CLError(ciErrNum);}

class CLInitialiser{
public:
	/*Class responsible for initialising the basic
	 *Opencl setup on the machine.
	 *@param{programSources] a list of string that specifies
	 *the file name */
	CLInitialiser(std::vector<std::string> programSources){
		try{	
			cl_int ciErrNum;
			/*Query the platform*/
			cl_platform_id platform;
			ciErrNum = clGetPlatformIDs(1, &platform, NULL);
			ERRCHK(ciErrNum, "Platform initialisation error");

			/*Query the target device*/
			cl_device_id device;
			ciErrNum = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &device, NULL);
			/*If GPU does not exist try it out on the CPU*/
			bool compiledForCpu = false;
			if (ciErrNum != CL_SUCCESS) {
				compiledForCpu = true;
				ciErrNum = clGetDeviceIDs(platform, CL_DEVICE_TYPE_CPU, 1, &device, NULL);
			}

			//assert(ciErrNum == CL_SUCCESS);
			ERRCHK(ciErrNum, "Device initialisation error");

			/*Context creation*/
			cl_context_properties cps[3] = {
				CL_CONTEXT_PLATFORM,
				(cl_context_properties) platform,
				0		
			};

			(*this).ctx = clCreateContext(cps, 1, &device, NULL, NULL, &ciErrNum);
			ERRCHK(ciErrNum , "Context creation error");
		
			/*Create the command queue*/
			(*this). commandQueue = clCreateCommandQueue(ctx, device, 0, &ciErrNum);
			ERRCHK(ciErrNum, "command queue creation error");

			/*Declare the buffer to store the program sources*/
			std::stringstream buffer;

			/*Go through the list of programs and find out if they exist*/		
			for(unsigned i = 0; i < programSources.size(); ++i){
				std::ifstream fin(programSources[i].c_str());
				if(!fin.is_open()) {
					/*Throw error or quit application*/
					std::cerr << "File " <<  programSources[i] << " not present" << std::endl;
					exit(1);
				}

				buffer << fin.rdbuf();
				buffer << std::endl;	
				fin.close();
			}

			std::string program = "";
			if(compiledForCpu){
				program.append("#define CPU 1\n");
			}
			program.append(buffer.str());

			const char * KernelSource = program.c_str();
			size_t *size = new size_t[1];
			size[0] = (size_t) (program.size() + 1);
			/*Create and build the program*/
			(*this).clProgram = clCreateProgramWithSource(
				ctx, 
				1, 
				(const char **) &KernelSource,
				size,
				&ciErrNum
			);
			delete size;

			std::stringstream ss;

			ss << "-cl-fast-relaxed-math";		
			std::string str;
			ss >> str;

			ciErrNum = clBuildProgram((*this).clProgram, 0, NULL, str.c_str(), NULL, NULL);
		
			if(ciErrNum != CL_SUCCESS){
				/* Shows the log */
				char* build_log;
				size_t log_size;
				/* First call to know the proper size*/
				clGetProgramBuildInfo(clProgram, device, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);
				build_log = new char[log_size+1];
				/* Second call to get the log*/
				clGetProgramBuildInfo(clProgram, device, CL_PROGRAM_BUILD_LOG, log_size, build_log, NULL);
				build_log[log_size] = '\0';
				std::cout << build_log << std::endl;
				std::string err(build_log);
				delete[] build_log;
				std::cerr << build_log << std::endl;
				exit(1);
			}
		
		} catch(CLError e){
			std::cerr << e.what() << std::endl;
			exit(1);
		}
	
	}

	~CLInitialiser(void){
		clReleaseCommandQueue(commandQueue);
		clReleaseContext(ctx);
		clReleaseProgram(clProgram);
	}

	cl_context ctx;
	cl_command_queue commandQueue;
	cl_program clProgram;
};

#endif
