#ifndef KERNEL_HPP
#define KERNEL_HPP
#if defined(__APPLE__) || defined(MACOSX)
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif
#include <string>
#include "CLError.hpp"
#include <vector>

class Kernel {
public:
	/*Kernel wrapper for OpenCL kernel
	 *@param[clProgram] reference to a program that
	 * has been created before.
	 *@param[commandQueue] reference to a queue through
	 * which communicatino between the GPU and CPU occurs.
	 *@param[kernelName] name of the kernel which will exist
	 * in the .cl file. The .cl file should have been compiled
	 * before
	 *@param[a] lower limit (k values for k dimensions)
	 *@param[b] upper limit (k values for k dimensions)
	 *@param[params] parameters to be used by certain kernel functions*/
	Kernel(
		cl_program clProgram,
		cl_command_queue commandQueue,
		cl_context ctx,
		std::string kernelName,
		const float *a,
		const float *b,
		int dim,
		const float *params,
		int paramLen,
		int n0,
		int n1,
		int n2,
		int reductionFactor,
		int y_init_size
	) : clProgram(clProgram), 
		commandQueue(commandQueue),
		ctx(ctx),
		kernelName(kernelName),
		a(a),
		b(b),
		params(params),
		dim(dim),
		n0(n0),
		n1(n1),
		n2(n2),
		reductionFactor(reductionFactor)
	{
		try{
				cl_int ciErrNum;
				kernel = clCreateKernel(clProgram, kernelName.c_str(), &ciErrNum);
				/*Quit program if kernel creation process fails*/
				if(ciErrNum != CL_SUCCESS) {
					std::cerr << "Error initialising kernel" << std::endl;
					exit(1);
				}	
	                    
				/*Allocate buffers in GPU memory for the device data*/
                d_y = clCreateBuffer(ctx, CL_MEM_READ_WRITE, y_init_size*sizeof(float), NULL, &ciErrNum);
                this->current_d_y_size = y_init_size;
 				
				if(ciErrNum != CL_SUCCESS) {
					std::cerr << "Error in creating buffer d_y" << std::endl;
					throw CLError(ciErrNum);
				}	
	
	
				/*Allocate buffer space for k lower bounds*/	
				d_a = clCreateBuffer(ctx, CL_MEM_READ_ONLY, sizeof(float)*dim, NULL, &ciErrNum);
				if(ciErrNum != CL_SUCCESS) {
						std::cerr << "Error in creating buffer d_a" << std::endl;
						throw CLError(ciErrNum);
				}       

				/*Allocate buffer space for k upper bounds*/
				d_b = clCreateBuffer(ctx, CL_MEM_READ_ONLY, sizeof(float)*dim, NULL, &ciErrNum);
				if(ciErrNum != CL_SUCCESS) {
						std::cerr << "Error in creating buffer d_b" << std::endl;
						throw CLError(ciErrNum);
				}       

				/*Allocate parameters if kernel uses parameters*/
				if(params != NULL){
					d_params = clCreateBuffer(ctx, CL_MEM_READ_ONLY, sizeof(float)*paramLen, NULL, &ciErrNum);
					if(ciErrNum != CL_SUCCESS) {
							std::cerr << "Error in creating buffer d_params" << std::endl;
							throw CLError(ciErrNum);
					}
				}
				
				/*Transfer the lower and upper bounds accross to the Device*/
				ciErrNum = clEnqueueWriteBuffer(commandQueue, d_a, CL_TRUE, 0, sizeof(float) * dim, (void *) a, 0, NULL, NULL);
				ciErrNum |= clEnqueueWriteBuffer(commandQueue, d_b, CL_TRUE, 0, sizeof(float) * dim, (void *) b, 0, NULL, NULL);
			
				/*Transfer the parameters if the kernel uses them*/
				if(params != NULL){
					ciErrNum |= clEnqueueWriteBuffer(commandQueue, d_params, CL_TRUE, 0, sizeof(float) * paramLen, (void *) params, 0, NULL, NULL);
				}

				if(ciErrNum != CL_SUCCESS){
						std::cerr << "Error copying data accross" << std::endl;
						throw CLError(ciErrNum);
				}


				/*Set up the arguments of the kernel*/
				ciErrNum = clSetKernelArg(this->kernel, 0, sizeof(cl_mem), &d_a);
				ciErrNum |= clSetKernelArg(this->kernel, 1, sizeof(cl_mem), &d_b);
				ciErrNum |= clSetKernelArg(this->kernel, 2, sizeof(cl_mem), &d_y);
				ciErrNum |= clSetKernelArg(this->kernel, 3, sizeof(int), &(*this).reductionFactor);
				ciErrNum |= clSetKernelArg(this->kernel, 4, sizeof(int), &n0);

				if(dim == 2){
					ciErrNum |= clSetKernelArg(this->kernel, 5, sizeof(int), &n1);
				} else if (dim == 3){
					ciErrNum |= clSetKernelArg(this->kernel, 5, sizeof(int), &n1);
					ciErrNum |= clSetKernelArg(this->kernel, 6, sizeof(int), &n2);
				}

				/*Set the parameters of the kernel if it requires any*/
				if((dim == 1) && (params != NULL)){	
					ciErrNum |= clSetKernelArg(this->kernel, 5, sizeof(cl_mem), &d_params);
				}
			
				if((dim == 2) && (params != NULL)){
					ciErrNum |= clSetKernelArg(this->kernel, 6, sizeof(cl_mem), &d_params);
				}

				if((dim == 3) && (params != NULL)){
					ciErrNum |= clSetKernelArg(this->kernel, 7, sizeof(cl_mem), &d_params);
				}

				if(ciErrNum != CL_SUCCESS){
					std::cerr << "Error in setting up kernel arguments in Kernel.hpp" << std::endl;
					throw CLError(ciErrNum);
				}

		} catch (CLError e){
			std::cerr << e.what() << std::endl;
			exit(e.getErrorCode());
		}

	}

	/*@param[reductionFactor] sets the kernel's reduction factor*/
	void setReductionFactor(int reductionFactor) throw(CLError){
		/*Set the reducionFactor of the object and the kernel*/
		cl_int ciErrNum;
		this->reductionFactor = reductionFactor;
		ciErrNum = clSetKernelArg(this->kernel, 3, sizeof(int), &(*this).reductionFactor);
		if(ciErrNum != CL_SUCCESS){
			throw(CLError(ciErrNum));
		}

		/*TODO: check for size of y buffer*/
	}

	/*@return the name of the kernel*/
	std::string getName(void) {return this->kernelName;}

	/*@param[hostData] a list of float pointers that the derived
	 * kernels classes should know about when transferring data
	 * back to the CPU*/
	void getData(std::vector<float *> hostData, unsigned size) throw(CLError){
		float *y = hostData[0];
		cl_int ciErrNum = clEnqueueReadBuffer(
					commandQueue,
					d_y,
					CL_TRUE,
					0,
					sizeof(float) * (current_d_y_size < size ? current_d_y_size : size),
					(void *)y,
					0,
					NULL,
					NULL);

		if(ciErrNum != CL_SUCCESS) {
			std::cerr <<"Could not retrieve data from GPU" << std::endl;
			std::cerr << CLError::descriptionOfError(ciErrNum) << std::endl;
			throw CLError(ciErrNum);
		}
	}


	/*@param[globalws] global work size of the launch configuration
	 *@param[localws] local work size of the launch configuration*/
	void launch(size_t *globalws, size_t *localws, int dim, unsigned size) throw(CLError) {
		cl_int ciErrNum;
		/*Check if the current d_y size allocated is
		 *enough for the kernel to be launched*/
		if(this->current_d_y_size < size) {
			this->setSize(size);
		}
		/*Launch the kernel*/
		ciErrNum = clEnqueueNDRangeKernel(
					commandQueue,
					kernel,
					dim,
					0,
					globalws,
					localws,
					0,
					0,
					0
		);

		if(ciErrNum != CL_SUCCESS){
			std::cerr << "Problems launching Kernel" << std::endl;
			throw CLError(ciErrNum);
		}
	}

	/*@param[size] new size of y to be set*/
	void setSize(unsigned size) throw (CLError){
		if(this->current_d_y_size < size){
			this->current_d_y_size = size;
			clReleaseMemObject(d_y);
			cl_int ciErrNum;
			d_y = clCreateBuffer(ctx, CL_MEM_READ_WRITE, current_d_y_size*sizeof(float), NULL, &ciErrNum);
			if(ciErrNum != CL_SUCCESS){
				throw(CLError(ciErrNum));
			}

			/*set the arguments again for the kernel*/
			ciErrNum = clSetKernelArg(this->kernel, 2, sizeof(cl_mem), &d_y);
			if(ciErrNum != CL_SUCCESS){
				throw(CLError(ciErrNum));
			}
		}
	}

	/*@param[n] value of the size of the problem*/
	void setN(int n){
		n0 = n;
		cl_int ciErrNum = clSetKernelArg(this->kernel, 4, sizeof(int), &n0);
		

		if(dim == 2){
			n1 = n;
			ciErrNum |= clSetKernelArg(this->kernel, 5, sizeof(int), &n1);
		} else if (dim == 3){
			n1 = n;
			n2 = n;
			ciErrNum |= clSetKernelArg(this->kernel, 5, sizeof(int), &n1);
			ciErrNum |= clSetKernelArg(this->kernel, 6, sizeof(int), &n2);
		}	

		if(ciErrNum != CL_SUCCESS){
			std::cerr << "Error in setting up kernel arguments in Kernel.hpp" << std::endl;
			throw CLError(ciErrNum);
		}			
	}

	virtual ~Kernel(void) {
		clReleaseKernel(kernel);
		clReleaseMemObject(d_a);
		clReleaseMemObject(d_b);
	}

protected:
	cl_kernel kernel;
	cl_program clProgram;
	cl_command_queue commandQueue;
	cl_context ctx;
	std::string kernelName;
	/*Host data*/
	const float *a;
	const float *b;
	const float *params;
	/*Device data*/
	cl_mem d_a;
	cl_mem d_b;
	cl_mem d_params;	
	cl_mem d_y;
	int dim;	
	int n0;
	int n1;
	int n2;
	int reductionFactor;
	unsigned current_d_y_size;
};

#endif
