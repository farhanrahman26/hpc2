#define X 0
#define Y 1
#define Z 2

__kernel void F0_GPU(__constant float *a, __constant float *b, __global float *y, int rf, int n0)
{
    int i0= get_global_id(X);
    //float u = (i0 + 0.5f)/n0;
	int i0Sub = i0*rf;
	__private float x[1];
	__private float acc = 0.0;
	for(unsigned ii0 = 0; ii0 < rf; ++ii0){
		x[0] = a[0]+(b[0]-a[0])*(i0Sub+ii0+0.5f)/n0;
		acc += native_exp(x[0]);
	}

    y[i0] = acc;
}

__kernel void F1_GPU(__constant float *a, __constant float *b, __global float *y, int rf, int n0, int n1, __constant float *params)
{
	int i0 = get_global_id(X);
	int i1 = get_global_id(Y);
	int i0Sub = i0*rf;
	int i1Sub = i1*rf;
	__private uint xdim = get_global_size(X);
	
	__private float x[2];
	__private float acc = 0.0;

	for(unsigned ii1 = 0; ii1 < rf; ++ii1){
		x[1] = a[1]+(b[1]-a[1]) * (i1Sub+ii1+0.5f)/n1;
		for(unsigned ii0 = 0; ii0 < rf; ++ii0){
			x[0] = a[0]+(b[0]-a[0]) * (i0Sub+ii0+0.5f)/n0;
			acc += native_sin(params[0] + x[0]*x[1]) * native_exp(params[1]+x[0]);
		}
	}	

//	y[get_global_id(X)+
//		mul24(get_global_id(Y),size)] = acc;

	y[i0 + i1*xdim] = acc;
}


__kernel void F2_GPU(__constant float *a, __constant float *b, __global float *y, int rf, int n0, int n1, int n2)
{
	int i0 = get_global_id(X);
	int i1 = get_global_id(Y);
	int i2 = get_global_id(Z);

	int i0Sub = i0*rf;
	int i1Sub = i1*rf;
	int i2Sub = i2*rf;
	__private uint xdim = get_global_size(X);
	__private uint ydim = get_global_size(Y);

	__private float x[3];
	__private float acc = 0.0;

	for(unsigned ii2 = 0; ii2 < rf; ++ii2){
		x[2]=a[2]+(b[2]-a[2]) * (ii2+i2Sub+0.5f)/n2;
		for(unsigned ii1 = 0; ii1 < rf; ++ii1){
			x[1]=a[1]+(b[1]-a[1]) * (ii1+i1Sub+0.5f)/n1;
			for(unsigned ii0 = 0; ii0 < rf; ++ii0){
				x[0]=a[0]+(b[0]-a[0]) * (ii0+i0Sub+0.5f)/n0;
				acc += round(native_exp(-x[0]))-round(native_exp(x[1]))*sin(x[2]);
			}
		}
	}

//	y[get_global_id(X)+
//		mul24(get_global_id(Y),size)+
//		mul24(get_global_id(Z),mul24(size,size))] = acc;

//	y[i0 + i1*n0 + i2*n0*n1] = round(exp(-x[0]))-round(exp(x[1]))*sin(x[2]);
	y[i0 + i1*xdim + i2*xdim*ydim] = acc;
}

__kernel void F3_GPU(__constant float *a, __constant float *b, __global float *y, int rf, int n0, int n1, int n2, __constant float *params)
{
	int i0 = get_global_id(X);
	int i1 = get_global_id(Y);
	int i2 = get_global_id(Z);

	int i0Sub = i0*rf;
	int i1Sub = i1*rf;
	int i2Sub = i2*rf;
	__private uint xdim = get_global_size(X);
	__private uint ydim = get_global_size(Y);

	__private float acc = 0.0;

	__private float x[3];

	__private float v = 0.0;

	for(unsigned ii2 = 0; ii2 < rf; ++ii2){
		x[2]=a[2]+(b[2]-a[2]) * (ii2+i2Sub+0.5f)/n2;
		for(unsigned ii1 = 0; ii1 < rf; ++ii1){
			x[1]=a[1]+(b[1]-a[1]) * (ii1+i1Sub+0.5f)/n1;
			for(unsigned ii0 = 0; ii0 < rf; ++ii0){
				x[0]=a[0]+(b[0]-a[0]) * (ii0+i0Sub+0.5f)/n0;
				#if CPU==1
				v=pow(x[0],params[0]) + pow(x[1],params[0]) + pow(x[2],params[0]);
				v=pow(v,1/params[0]);
				#else
				v=native_powr(x[0],params[0]) + native_powr(x[1],params[0]) + native_powr(x[2],params[0]);
				v=native_powr(v,1/params[0]);
				#endif
				acc += native_sin(v)/v;
			}
		}
	}

	y[i0 + i1*xdim + i2*xdim*ydim] = acc;

//	__private float v=pow(x[0],params[0]) + pow(x[1],params[0]) + pow(x[2],params[0]);
//	v=pow(v,1/params[0]);
//	y[i0 + i1*n0 + i2*n0*n1] = sin(v)/v;
}


__kernel void F4_GPU(__constant float *a, __constant float *b, __global float *y, int rf, int n0, int n1, int n2, __constant float *params)
{
	int i0 = get_global_id(X);
	int i1 = get_global_id(Y);
	int i2 = get_global_id(Z);
	int i0Sub = i0*rf;
	int i1Sub = i1*rf;
	int i2Sub = i2*rf;

	__private float x[3];
	__private float temp = 0.0f;
	__private float acc = 0.0f;
	__private uint xdim = get_global_size(X);
	__private uint ydim = get_global_size(Y);

	for(unsigned ii2 = 0; ii2 < rf; ++ii2){
		x[2]=a[2]+(b[2]-a[2]) * (ii2+i2Sub+0.5f)/n2;
		for(unsigned ii1 = 0; ii1 < rf; ++ii1){
			x[1]=a[1]+(b[1]-a[1]) * (ii1+i1Sub+0.5f)/n1;
			for(unsigned ii0 = 0; ii0 < rf; ++ii0){
				temp = 0.0f;
				x[0]=a[0]+(b[0]-a[0]) * (ii0+i0Sub+0.5f)/n0;
				temp += (params[0]*x[0] + params[1]*x[1] + params[2]*x[2]) * x[0];
				temp += (params[3]*x[0] + params[4]*x[1] + params[5]*x[2]) * x[1];
				temp += (params[6]*x[0] + params[7]*x[1] + params[8]*x[2]) * x[2];
				acc += params[9] * native_exp(-temp/ 2.0f);
			}
		}
	}
//	y[i0 + i1*n0 + i2*n0*n1] = params[9] * exp(-acc / 2.0f);
	y[i0 + i1*xdim + i2*xdim*ydim] = acc;
}

__kernel void F5_GPU(__constant float *a, __constant float *b, __global float *y, int rf, int n0, int n1, int n2)
{
	int i0 = get_global_id(X);
	int i1 = get_global_id(Y);
	int i2 = get_global_id(Z);
	int i0Sub = i0*rf;
	int i1Sub = i1*rf;
	int i2Sub = i2*rf;
	
	__private float x[3];
	__private float acc = 0.0f;
	__private uint xdim = get_global_size(X);
	__private uint ydim = get_global_size(Y);
	__private float temp;

	for(unsigned ii2 = 0; ii2 < rf; ++ii2){
		x[2]=a[2]+(b[2]-a[2]) * (ii2+i2Sub+0.5f)/n2;
		for(unsigned ii1 = 0; ii1 < rf; ++ii1){
			x[1]=a[1]+(b[1]-a[1]) * (ii1+i1Sub+0.5f)/n1;
			for(unsigned ii0 = 0; ii0 < rf; ++ii0){ 
				x[0]=a[0]+(b[0]-a[0]) * (ii0+i0Sub+0.5f)/n0;
				#if CPU == 1
				temp = native_sin(pow(x[0],x[1]));
				acc += pow(temp*temp, x[2]);				
				#else
				temp = native_sin(native_powr(x[0],x[1]));
				acc += native_powr(temp*temp, x[2]);
				#endif
			}
		}
	}

//	y[i0 + i1*n0 + i2*n0*n1] = pow(pow(sin( pow(x[0], x[1]) ), 2.0f), x[2]);
	y[i0 + i1*xdim + i2*xdim*ydim] = acc;
}

__kernel void F6_GPU(__constant float *a, __constant float *b, __global float *y, int rf, int n0, int n1, int n2, __constant float *params)
{
	int i0 = get_global_id(X);
	int i1 = get_global_id(Y);
	int i2 = get_global_id(Z);
	int i0Sub = i0*rf;
	int i1Sub = i1*rf;
	int i2Sub = i2*rf;

	__private float x[3];
	__private float acc = 0.0f;
	__private uint xdim = get_global_size(X);
	__private uint ydim = get_global_size(Y);
	__private float _a = 0.0f;
	__private float d = 0.0f;

	for(unsigned ii2 = 0; ii2 < rf; ++ii2){
		x[2]=a[2]+(b[2]-a[2]) * (ii2+i2Sub+0.5f)/n2;
		for(unsigned ii1 = 0; ii1 < rf; ++ii1){
			x[1]=a[1]+(b[1]-a[1]) * (ii1+i1Sub+0.5f)/n1;
			for(unsigned ii0 = 0; ii0 < rf; ++ii0){
				x[0]=a[0]+(b[0]-a[0]) * (ii0+i0Sub+0.5f)/n0;
				#if CPU == 1
				_a= pow(x[0]*x[0]+ x[1]*x[1] + x[2]*x[2],0.5f);
				#else
				_a= native_powr(x[0]*x[0]+ x[1]*x[1] + x[2]*x[2],0.5f);
				#endif
				d = fabs(_a-params[0]);
				acc += d < params[1];
			}
		}
	}

//	__private float _a= pow(pow(x[0],2.0f)+pow(x[1],2.0f)+pow(x[2],2.0f),0.5f);
//	__private float d= fabs(_a-params[0]);

//	y[i0 + i1*n0 + i2*n0*n1] = d < params[1];
	y[i0 + i1*xdim + i2*xdim*ydim] = acc;
}


/*Kernels for Simpson's rule*/
__kernel void F0_GPU_SIMP(__constant float *a, __constant float *b, __global float *y, int rf, int n0){
    int i0= get_global_id(X);
	int i0Sub = i0*rf;

	__private float acc = 0.0f;
	__private float xn, xnp1;
	__private float stride = (b[0]-a[0])/(n0*1.0f);
	__private float mid = 0.0f;
	for (unsigned ii0 = 0; ii0 < rf; ++ii0){
		xn = a[0] + ((float) i0Sub+ii0)*stride;
		xnp1 = xn+stride;
		mid = (xn+xnp1)/2.0f;
		acc += (native_exp(xn) + 4* native_exp(mid) + native_exp(xnp1));
	}
	y[i0] = acc;	
}

inline float F1Simp(float x, float y, float param0, float param1){
	return native_sin(param0 + x*y) * native_exp(param1+x);
}


__kernel void F1_GPU_SIMP(__constant float *a, __constant float *b, __global float *y, int rf, int n0, int n1, __constant float *params){
	int i0 = get_global_id(X);
	int i1 = get_global_id(Y);
	int i0Sub = i0*rf;
	int i1Sub = i1*rf;
	__private uint xdim = get_global_size(X);

	__private float acc = 0.0;
	__private float xn, xnp1;
	__private float yn, ynp1;
	__private float xstride = (b[0]-a[0])/(n0*1.0);
	__private float ystride = (b[1]-a[1])/(n1*1.0);
	__private float midX = 0.0;
	__private float midY = 0.0;
	__private float param0 = params[0];
	__private float param1 = params[1];

	for(unsigned ii1 = 0; ii1 < rf; ++ii1){
			yn = a[1] + ((float) ii1+i1Sub)*ystride;		
			ynp1 = yn+ystride;
			midY = (yn+ynp1)/2.0;
			for (unsigned ii0 = 0; ii0 < rf; ++ii0){
				xn = a[0] + ((float) ii0+i0Sub)*xstride;
				xnp1 = xn+xstride;
				midX = (xn+xnp1)/2.0;
				acc += F1Simp(xn,ynp1,param0, param1) + 4.0*F1Simp(midX,ynp1,param0, param1) + F1Simp(xnp1,ynp1,param0, param1); 
				acc += 4.0*F1Simp(xn,midY,param0, param1) + 16.0*F1Simp(midX,midY,param0, param1) + 4.0*F1Simp(xnp1,midY,param0, param1);
				acc += F1Simp(xn,yn,param0, param1) + 4.0*F1Simp(midX,yn,param0, param1) + F1Simp(xnp1,yn,param0, param1);
			}
	}
	y[i0 + i1*xdim] = acc;
}

inline float F2_S(const float x, const float y, const float z)
{
	return round(native_exp(-x)) - round(native_exp(y)) * native_sin(z);
}

__kernel void F2_GPU_SIMP(__constant float *a, __constant float *b, __global float *y, int rf, int n0, int n1, int n2){
	int i0 = get_global_id(X);
	int i1 = get_global_id(Y);
	int i2 = get_global_id(Z);

	int i0Sub = i0*rf;
	int i1Sub = i1*rf;
	int i2Sub = i2*rf;
	__private uint xdim = get_global_size(X);
	__private uint ydim = get_global_size(Y);

	__private float acc = 0.0;
	__private float xn, xnp1;
	__private float yn, ynp1;
	__private float zn, znp1;
	__private float xstride = (b[0]-a[0])/(n0*1.0);
	__private float ystride = (b[1]-a[1])/(n1*1.0);
	__private float zstride = (b[2]-a[2])/(n2*1.0);
	__private float midX = 0.0;
	__private float midY = 0.0;
	__private float midZ = 0.0;
	for(unsigned ii2 = 0; ii2 < rf; ++ii2){
		zn = a[2] + ((float) ii2+i2Sub)*zstride;
		znp1 = zn + zstride;
		midZ = (zn+znp1)/2.0;
		for(unsigned ii1 = 0; ii1 < rf; ++ii1){
				yn = a[1] + ((float) ii1+i1Sub)*ystride;		
				ynp1 = yn+ystride;
				midY = (yn+ynp1)/2.0;
			for (unsigned ii0 = 0; ii0 < rf; ++ii0){
				xn = a[0] + ((float) ii0+i0Sub)*xstride;
				xnp1 = xn+xstride;
				midX = (xn+xnp1)/2.0;
				acc += 1.0*F2_S(xn,ynp1,zn) + 4.0*F2_S(midX,ynp1,zn) + 1.0*F2_S(xnp1,ynp1,zn);					
				acc += 4.0*F2_S(xn,midY,zn) + 16.0*F2_S(midX,midY,zn) + 4.0*F2_S(xnp1,midY,zn);
				acc += 1.0*F2_S(xn,yn,zn) + 4.0*F2_S(midX,yn,zn) + 1.0*F2_S(xnp1,yn,zn);

				acc += 4.0*F2_S(xn,ynp1,midZ) + 16.0*F2_S(midX,ynp1,midZ) + 4.0*F2_S(xnp1,ynp1,midZ);					
				acc += 16.0*F2_S(xn,midY,midZ) + 64.0*F2_S(midX,midY,midZ) + 16.0*F2_S(xnp1,midY,midZ);
				acc += 4.0*F2_S(xn,yn,midZ) + 16.0*F2_S(midX,yn,midZ) + 4.0*F2_S(xnp1,yn,midZ);

				acc += 1.0*F2_S(xn,ynp1,znp1) + 4.0*F2_S(midX,ynp1,znp1) + 1.0*F2_S(xnp1,ynp1,znp1);					
				acc += 4.0*F2_S(xn,midY,znp1) + 16.0*F2_S(midX,midY,znp1) + 4.0*F2_S(xnp1,midY,znp1);
				acc += 1.0*F2_S(xn,yn,znp1) + 4.0*F2_S(midX,yn,znp1) + 1.0*F2_S(xnp1,yn,znp1);
			}
		}
	}

	y[i0 + i1*xdim + i2*xdim*ydim] = acc;
}

inline float F3_S(const float x, const float y, const float z, float param0)
{
	#if CPU==1
	// Perform a vector norm of a specific power, e.g. if params[0]==2 then it is the euclidian norm
	float v= pow(x,param0) + pow(y,param0) + pow(z,param0);
	v= pow(v,1/param0);
	#else
	// Perform a vector norm of a specific power, e.g. if params[0]==2 then it is the euclidian norm
	float v= native_powr(x,param0) + native_powr(y,param0) + native_powr(z,param0);
	v= native_powr(v,1/param0);
	#endif
	// Then a sinc
	return v == 0.0 ? 1 : native_divide(native_sin(v),v);
}

__kernel void F3_GPU_SIMP(__constant float *a, __constant float *b, __global float *y, int rf, int n0, int n1, int n2, __constant float *params)
{
	int i0 = get_global_id(X);
	int i1 = get_global_id(Y);
	int i2 = get_global_id(Z);

	int i0Sub = i0*rf;
	int i1Sub = i1*rf;
	int i2Sub = i2*rf;
	__private uint xdim = get_global_size(X);
	__private uint ydim = get_global_size(Y);

	__private float acc = 0.0;
	__private float xn, xnp1;
	__private float yn, ynp1;
	__private float zn, znp1;
	__private float xstride = (b[0]-a[0])/(n0*1.0);
	__private float ystride = (b[1]-a[1])/(n1*1.0);
	__private float zstride = (b[2]-a[2])/(n2*1.0);
	__private float midX = 0.0;
	__private float midY = 0.0;
	__private float midZ = 0.0;
	__private float param0 = params[0];
	for(unsigned ii2 = 0; ii2 < rf; ++ii2){
		zn = a[2] + ((float) ii2+i2Sub)*zstride;
		znp1 = zn + zstride;
		midZ = (zn+znp1)/2.0;
		for(unsigned ii1 = 0; ii1 < rf; ++ii1){
				yn = a[1] + ((float) ii1+i1Sub)*ystride;		
				ynp1 = yn+ystride;
				midY = (yn+ynp1)/2.0;
			for (unsigned ii0 = 0; ii0 < rf; ++ii0){
				xn = a[0] + ((float) ii0+i0Sub)*xstride;
				xnp1 = xn+xstride;
				midX = (xn+xnp1)/2.0;
				acc += 1.0*F3_S(xn,ynp1,zn,param0) + 4.0*F3_S(midX,ynp1,zn,param0) + 1.0*F3_S(xnp1,ynp1,zn,param0);					
				acc += 4.0*F3_S(xn,midY,zn,param0) + 16.0*F3_S(midX,midY,zn,param0) + 4.0*F3_S(xnp1,midY,zn,param0);
				acc += 1.0*F3_S(xn,yn,zn,param0) + 4.0*F3_S(midX,yn,zn,param0) + 1.0*F3_S(xnp1,yn,zn,param0);

				acc += 4.0*F3_S(xn,ynp1,midZ,param0) + 16.0*F3_S(midX,ynp1,midZ,param0) + 4.0*F3_S(xnp1,ynp1,midZ,param0);					
				acc += 16.0*F3_S(xn,midY,midZ,param0) + 64.0*F3_S(midX,midY,midZ,param0) + 16.0*F3_S(xnp1,midY,midZ,param0);
				acc += 4.0*F3_S(xn,yn,midZ,param0) + 16.0*F3_S(midX,yn,midZ,param0) + 4.0*F3_S(xnp1,yn,midZ,param0);

				acc += 1.0*F3_S(xn,ynp1,znp1,param0) + 4.0*F3_S(midX,ynp1,znp1,param0) + 1.0*F3_S(xnp1,ynp1,znp1,param0);					
				acc += 4.0*F3_S(xn,midY,znp1,param0) + 16.0*F3_S(midX,midY,znp1,param0) + 4.0*F3_S(xnp1,midY,znp1,param0);
				acc += 1.0*F3_S(xn,yn,znp1,param0) + 4.0*F3_S(midX,yn,znp1,param0) + 1.0*F3_S(xnp1,yn,znp1,param0);
			}
		}
	}

	y[i0 + i1*xdim + i2*xdim*ydim] = acc;

}

inline float F4_S(const float x, const float y, const float z, 
	const float param0,
	const float param1,
	const float param2,
	const float param3,
	const float param4,
	const float param5,
	const float param6,
	const float param7,
	const float param8,
	const float param9)
{
	__private float temp = 0.0f;
	temp += (param0*x + param1*y + param2*z) * x;
	temp += (param3*x + param4*y + param5*z) * y;
	temp += (param6*x + param7*y + param8*z) * z;
	return param9 * native_exp(-temp/ 2.0f);
}

__kernel void F4_GPU_SIMP(__constant float *a, __constant float *b, __global float *y, int rf, int n0, int n1, int n2, __constant float *params)
{
	int i0 = get_global_id(X);
	int i1 = get_global_id(Y);
	int i2 = get_global_id(Z);
	int i0Sub = i0*rf;
	int i1Sub = i1*rf;
	int i2Sub = i2*rf;

	__private uint xdim = get_global_size(X);
	__private uint ydim = get_global_size(Y);

	__private float acc = 0.0;
	__private float xn, xnp1;
	__private float yn, ynp1;
	__private float zn, znp1;
	__private float xstride = (b[0]-a[0])/(n0*1.0);
	__private float ystride = (b[1]-a[1])/(n1*1.0);
	__private float zstride = (b[2]-a[2])/(n2*1.0);
	__private float midX = 0.0;
	__private float midY = 0.0;
	__private float midZ = 0.0;

	__private float param0 = params[0];
	__private float param1 = params[1];
	__private float param2 = params[2];
	__private float param3 = params[3];
	__private float param4 = params[4];
	__private float param5 = params[5];
	__private float param6 = params[6];
	__private float param7 = params[7];
	__private float param8 = params[8];
	__private float param9 = params[9];

	for(unsigned ii2 = 0; ii2 < rf; ++ii2){
		zn = a[2] + ((float) ii2+i2Sub)*zstride;
		znp1 = zn + zstride;
		midZ = (zn+znp1)/2.0;
		for(unsigned ii1 = 0; ii1 < rf; ++ii1){
				yn = a[1] + ((float) ii1+i1Sub)*ystride;		
				ynp1 = yn+ystride;
				midY = (yn+ynp1)/2.0;
			for (unsigned ii0 = 0; ii0 < rf; ++ii0){
				xn = a[0] + ((float) ii0+i0Sub)*xstride;
				xnp1 = xn+xstride;
				midX = (xn+xnp1)/2.0;
				acc += 1.0*F4_S(xn,ynp1,zn,param0,param1,param2,param3,param4,param5,param6,param7,param8,param9) + 4.0*F4_S(midX,ynp1,zn,param0,param1,param2,param3,param4,param5,param6,param7,param8,param9) + 1.0*F4_S(xnp1,ynp1,zn,param0,param1,param2,param3,param4,param5,param6,param7,param8,param9);					
				acc += 4.0*F4_S(xn,midY,zn,param0,param1,param2,param3,param4,param5,param6,param7,param8,param9) + 16.0*F4_S(midX,midY,zn,param0,param1,param2,param3,param4,param5,param6,param7,param8,param9) + 4.0*F4_S(xnp1,midY,zn,param0,param1,param2,param3,param4,param5,param6,param7,param8,param9);
				acc += 1.0*F4_S(xn,yn,zn,param0,param1,param2,param3,param4,param5,param6,param7,param8,param9) + 4.0*F4_S(midX,yn,zn,param0,param1,param2,param3,param4,param5,param6,param7,param8,param9) + 1.0*F4_S(xnp1,yn,zn,param0,param1,param2,param3,param4,param5,param6,param7,param8,param9);

				acc += 4.0*F4_S(xn,ynp1,midZ,param0,param1,param2,param3,param4,param5,param6,param7,param8,param9) + 16.0*F4_S(midX,ynp1,midZ,param0,param1,param2,param3,param4,param5,param6,param7,param8,param9) + 4.0*F4_S(xnp1,ynp1,midZ,param0,param1,param2,param3,param4,param5,param6,param7,param8,param9);					
				acc += 16.0*F4_S(xn,midY,midZ,param0,param1,param2,param3,param4,param5,param6,param7,param8,param9) + 64.0*F4_S(midX,midY,midZ,param0,param1,param2,param3,param4,param5,param6,param7,param8,param9) + 16.0*F4_S(xnp1,midY,midZ,param0,param1,param2,param3,param4,param5,param6,param7,param8,param9);
				acc += 4.0*F4_S(xn,yn,midZ,param0,param1,param2,param3,param4,param5,param6,param7,param8,param9) + 16.0*F4_S(midX,yn,midZ,param0,param1,param2,param3,param4,param5,param6,param7,param8,param9) + 4.0*F4_S(xnp1,yn,midZ,param0,param1,param2,param3,param4,param5,param6,param7,param8,param9);

				acc += 1.0*F4_S(xn,ynp1,znp1,param0,param1,param2,param3,param4,param5,param6,param7,param8,param9) + 4.0*F4_S(midX,ynp1,znp1,param0,param1,param2,param3,param4,param5,param6,param7,param8,param9) + 1.0*F4_S(xnp1,ynp1,znp1,param0,param1,param2,param3,param4,param5,param6,param7,param8,param9);					
				acc += 4.0*F4_S(xn,midY,znp1,param0,param1,param2,param3,param4,param5,param6,param7,param8,param9) + 16.0*F4_S(midX,midY,znp1,param0,param1,param2,param3,param4,param5,param6,param7,param8,param9) + 4.0*F4_S(xnp1,midY,znp1,param0,param1,param2,param3,param4,param5,param6,param7,param8,param9);
				acc += 1.0*F4_S(xn,yn,znp1,param0,param1,param2,param3,param4,param5,param6,param7,param8,param9) + 4.0*F4_S(midX,yn,znp1,param0,param1,param2,param3,param4,param5,param6,param7,param8,param9) + 1.0*F4_S(xnp1,yn,znp1,param0,param1,param2,param3,param4,param5,param6,param7,param8,param9);
			}
		}
	}

	y[i0 + i1*xdim + i2*xdim*ydim] = acc;
}

inline float F5_S(const float x, const float y, const float z)
{
	#if CPU==1
	__private float pw = pow(x,y);
	__private float temp = native_sin(pw);
	temp*=temp;
	return pow(temp,z);
	#else
	__private float pw = native_powr(x,y);
	__private float temp = native_sin(pw);
	temp*=temp;
	return native_powr(temp,z);
	#endif
}

__kernel void F5_GPU_SIMP(__constant float *a, __constant float *b, __global float *y, int rf, int n0, int n1, int n2)
{
	int i0 = get_global_id(X);
	int i1 = get_global_id(Y);
	int i2 = get_global_id(Z);
	int i0Sub = i0*rf;
	int i1Sub = i1*rf;
	int i2Sub = i2*rf;

	__private uint xdim = get_global_size(X);
	__private uint ydim = get_global_size(Y);

	__private float acc = 0.0;
	__private float xn, xnp1;
	__private float yn, ynp1;
	__private float zn, znp1;
	__private float xstride = (b[0]-a[0])/(n0*1.0);
	__private float ystride = (b[1]-a[1])/(n1*1.0);
	__private float zstride = (b[2]-a[2])/(n2*1.0);
	__private float midX = 0.0;
	__private float midY = 0.0;
	__private float midZ = 0.0;

	for(unsigned ii2 = 0; ii2 < rf; ++ii2){
		zn = a[2] + ((float) ii2+i2Sub)*zstride;
		znp1 = zn + zstride;
		midZ = (zn+znp1)/2.0;
		for(unsigned ii1 = 0; ii1 < rf; ++ii1){
				yn = a[1] + ((float) ii1+i1Sub)*ystride;		
				ynp1 = yn+ystride;
				midY = (yn+ynp1)/2.0;
			for (unsigned ii0 = 0; ii0 < rf; ++ii0){
				xn = a[0] + ((float) ii0+i0Sub)*xstride;
				xnp1 = xn+xstride;
				midX = (xn+xnp1)/2.0;
				acc += 1.0*F5_S(xn,ynp1,zn) + 4.0*F5_S(midX,ynp1,zn) + 1.0*F5_S(xnp1,ynp1,zn);					
				acc += 4.0*F5_S(xn,midY,zn) + 16.0*F5_S(midX,midY,zn) + 4.0*F5_S(xnp1,midY,zn);
				acc += 1.0*F5_S(xn,yn,zn) + 4.0*F5_S(midX,yn,zn) + 1.0*F5_S(xnp1,yn,zn);

				acc += 4.0*F5_S(xn,ynp1,midZ) + 16.0*F5_S(midX,ynp1,midZ) + 4.0*F5_S(xnp1,ynp1,midZ);					
				acc += 16.0*F5_S(xn,midY,midZ) + 64.0*F5_S(midX,midY,midZ) + 16.0*F5_S(xnp1,midY,midZ);
				acc += 4.0*F5_S(xn,yn,midZ) + 16.0*F5_S(midX,yn,midZ) + 4.0*F5_S(xnp1,yn,midZ);

				acc += 1.0*F5_S(xn,ynp1,znp1) + 4.0*F5_S(midX,ynp1,znp1) + 1.0*F5_S(xnp1,ynp1,znp1);					
				acc += 4.0*F5_S(xn,midY,znp1) + 16.0*F5_S(midX,midY,znp1) + 4.0*F5_S(xnp1,midY,znp1);
				acc += 1.0*F5_S(xn,yn,znp1) + 4.0*F5_S(midX,yn,znp1) + 1.0*F5_S(xnp1,yn,znp1);
			}
		}
	}

	y[i0 + i1*xdim + i2*xdim*ydim] = acc;
}

float F6_S(const float x, const float y, const float z, const float param0, const float param1){
	#if CPU==1
	__private float _a = 0.0f;
	__private float d = 0.0f;
	_a= pow(x*x + y*y + z*z,0.5f);
	d = fabs(_a-param0);
	return (float) d < param1;
	#else
	__private float _a = 0.0f;
	__private float d = 0.0f;
	_a= native_powr(x*x + y*y + z*z,0.5f);
	d = fabs(_a-param0);
	return (float) d < param1;
	#endif
}

__kernel void F6_GPU_SIMP(__constant float *a, __constant float *b, __global float *y, int rf, int n0, int n1, int n2, __constant float *params)
{
	int i0 = get_global_id(X);
	int i1 = get_global_id(Y);
	int i2 = get_global_id(Z);
	int i0Sub = i0*rf;
	int i1Sub = i1*rf;
	int i2Sub = i2*rf;

	__private float acc = 0.0f;
	__private uint xdim = get_global_size(X);
	__private uint ydim = get_global_size(Y);

	__private float xn, xnp1;
	__private float yn, ynp1;
	__private float zn, znp1;
	__private float xstride = (b[0]-a[0])/(n0*1.0);
	__private float ystride = (b[1]-a[1])/(n1*1.0);
	__private float zstride = (b[2]-a[2])/(n2*1.0);
	__private float midX = 0.0;
	__private float midY = 0.0;
	__private float midZ = 0.0;
	__private float param0 = params[0];
	__private float param1 = params[1];
	for(unsigned ii2 = 0; ii2 < rf; ++ii2){
		zn = a[2] + ((float) ii2+i2Sub)*zstride;
		znp1 = zn + zstride;
		midZ = (zn+znp1)/2.0;
		for(unsigned ii1 = 0; ii1 < rf; ++ii1){
				yn = a[1] + ((float) ii1+i1Sub)*ystride;		
				ynp1 = yn+ystride;
				midY = (yn+ynp1)/2.0;
			for (unsigned ii0 = 0; ii0 < rf; ++ii0){
				xn = a[0] + ((float) ii0+i0Sub)*xstride;
				xnp1 = xn+xstride;
				midX = (xn+xnp1)/2.0;
				acc += 1.0*F6_S(xn,ynp1,zn,param0,param1) + 4.0*F6_S(midX,ynp1,zn,param0,param1) + 1.0*F6_S(xnp1,ynp1,zn,param0,param1);					
				acc += 4.0*F6_S(xn,midY,zn,param0,param1) + 16.0*F6_S(midX,midY,zn,param0,param1) + 4.0*F6_S(xnp1,midY,zn,param0,param1);
				acc += 1.0*F6_S(xn,yn,zn,param0,param1) + 4.0*F6_S(midX,yn,zn,param0,param1) + 1.0*F6_S(xnp1,yn,zn,param0,param1);

				acc += 4.0*F6_S(xn,ynp1,midZ,param0,param1) + 16.0*F6_S(midX,ynp1,midZ,param0,param1) + 4.0*F6_S(xnp1,ynp1,midZ,param0,param1);					
				acc += 16.0*F6_S(xn,midY,midZ,param0,param1) + 64.0*F6_S(midX,midY,midZ,param0,param1) + 16.0*F6_S(xnp1,midY,midZ,param0,param1);
				acc += 4.0*F6_S(xn,yn,midZ,param0,param1) + 16.0*F6_S(midX,yn,midZ,param0,param1) + 4.0*F6_S(xnp1,yn,midZ,param0,param1);

				acc += 1.0*F6_S(xn,ynp1,znp1,param0,param1) + 4.0*F6_S(midX,ynp1,znp1,param0,param1) + 1.0*F6_S(xnp1,ynp1,znp1,param0,param1);					
				acc += 4.0*F6_S(xn,midY,znp1,param0,param1) + 16.0*F6_S(midX,midY,znp1,param0,param1) + 4.0*F6_S(xnp1,midY,znp1,param0,param1);
				acc += 1.0*F6_S(xn,yn,znp1,param0,param1) + 4.0*F6_S(midX,yn,znp1,param0,param1) + 1.0*F6_S(xnp1,yn,znp1,param0,param1);
			}
		}
	}

	y[i0 + i1*xdim + i2*xdim*ydim] = acc;

}




