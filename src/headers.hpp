#ifndef HEADERS_HPP
#define HEADERS_HPP

#if defined(__APPLE__) || defined(MACOSX)
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif
#include "InvalidDimException.hpp"
#include "Integrator1D.hpp"
#include "Integrator1DSimp.hpp"
#include "Integrator2D.hpp"
#include "Integrator2DSimp.hpp"
#include "Integrator3D.hpp"
#include "Integrator3DSimp.hpp"
#include "CLInitialiser.hpp"
#include "KernelF0.hpp"
#include "KernelF1.hpp"
#include "KernelF2.hpp"
#include "KernelF3.hpp"
#include "KernelF4.hpp"
#include "KernelF5.hpp"
#include "KernelF6.hpp"

#endif