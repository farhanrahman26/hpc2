#ifndef INTEGRATOR_HPP
#define INTEGRATOR_HPP
#include "InvalidDimException.hpp"
#include "Kernel.hpp"
#include "KernelF0.hpp"


class Integrator {
public:
	/**Constructor for Integration
	 * @param[functionCode] identifies the dimensionality of the function (k)
     * @param[a] an array of k lower bound values
	 * @param[b] an array of k upper bound values
	 * @param[eps] target accuracy to be reached |F_t - F_a| < eps where F_t is the true value
	 * @param[errorEstimate] estimated error in integral*/
	Integrator(
		int functionCode,
		const float *a,
		const float *b,
		float eps,
		const float *params,
		float *errorEstimate

	) : functionCode(functionCode),
		a(a),
		b(b),
		eps(eps),
		params(params),
		errorEstimate(errorEstimate) {}

	virtual ~Integrator(void){}

	/**Method to be implemented by
	 * by subclasses integrating functions
	 * of different dimensionality*/
	virtual double integrate(int n, Kernel &kernel) = 0;

protected:
	int functionCode;
	const float *a;
	const float *b;
	float eps;
	const float *params;
	float *errorEstimate;
};


#endif
