#ifndef INTEGRATOR_3D_HPP
#define INTEGRATOR_3D_HPP
#include "Integrator.hpp"
#include "InvalidDimException.hpp"

class Integrator3D : public Integrator {
public:
	Integrator3D(
		int functionCode,
		const float *a,
		const float *b,
		float eps,
		const float *params,
		float *errorEstimate
	) throw (InvalidDimException) : Integrator(functionCode, a, b, eps, params, errorEstimate) {
			/*Check for dimensionality mismatch*/
			if (functionCode <= 1){
				throw InvalidDimException(functionCode, 3);
			}	
	}

	/*Integration 2 dimensional*/
	double integrate(int n, Kernel &kernel) {
		unsigned n0 = n;
		unsigned n1 = n;
		unsigned n2 = n;
		float acc = 0.0f;
		float *y = new float[n0*n1*n1];

		const int targetReductionFactor = 64;
		const int reductionFactor = n < targetReductionFactor ? 1 : targetReductionFactor;

//		size_t localws[3] = {1,1,1};
		size_t globalws[3] = {n0/reductionFactor, n1/reductionFactor, n2/reductionFactor};

		std::vector<float *> hostData;
		hostData.push_back(y);

		kernel.setReductionFactor(reductionFactor);

		try{
			kernel.launch(globalws, NULL, 3, n0*n1*n2/(reductionFactor*reductionFactor*reductionFactor));
		} catch(CLError e){
			std::cerr << e.what() << std::endl;
			exit(e.getErrorCode());
		}

		kernel.getData(hostData, n0*n1*n2/(reductionFactor*reductionFactor*reductionFactor));
		
		for(unsigned i = 0; i < n0*n1*n2/(reductionFactor*reductionFactor*reductionFactor); ++i){
			acc += y[i];
		}

		acc *= (b[0]-a[0])*(b[1]-a[1])*(b[2]-a[2]);

		delete y;	
		return acc/(n0*n1*n2);

	}	

};
#endif
