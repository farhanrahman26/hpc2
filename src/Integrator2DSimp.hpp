#ifndef INTEGRATOR_2D_SIMP
#define INTEGRATOR_2D_SIMP
#include "Integrator.hpp"
#include <math.h>

class Integrator2DSimp : public Integrator {
public:
	Integrator2DSimp(
		int functionCode,
		const float *a,
		const float *b,
		float eps,
		const float *params,
		float *errorEstimate
	) throw (InvalidDimException) : Integrator(functionCode, a, b, eps, params, errorEstimate) {
			/*Check for dimensionality mismatch*/
			if (functionCode != 1){
				throw InvalidDimException(functionCode, 2);
			}	
	}

	/*Integration 2 dimensional*/
	double integrate(int n, Kernel &kernel) {
		unsigned n0 = n;
		unsigned n1 = n;
		float acc = 0.0f;
		float *x = new float[2];
		float *y = new float[n0*n1];

//		const unsigned threadsPerBlock = 16;
		const int targetReductionFactor = 64;
		const int reductionFactor = n < targetReductionFactor ? 1 : targetReductionFactor;
//		size_t localws[2] = {n1 < threadsPerBlock ? 1 : threadsPerBlock,  n0 < threadsPerBlock ? 1 : threadsPerBlock};
		size_t globalws[2] = {n1/reductionFactor, n0/reductionFactor};

		std::vector<float *> hostData;
		hostData.push_back(y);

		kernel.setReductionFactor(reductionFactor);

		try{
			kernel.launch(globalws, NULL, 2, n0*n1/(reductionFactor*reductionFactor));
		} catch(CLError e){
			std::cerr << e.what() << std::endl;
			exit(e.getErrorCode());
		}

		kernel.getData(hostData, n0*n1/(reductionFactor*reductionFactor));
		
		for(unsigned i = 0; i < n0*n1/(reductionFactor*reductionFactor); ++i){
			acc += y[i];
		}

		delete y;
		delete x;
	
		return acc*((b[0]-a[0]))*((b[1]-a[1]))/(n*n*36.0);

	}	
};


#endif
