#ifndef INTEGRATE_HPP
#define INTEGRATE_HPP

/*@param[functionCode] The code of the function to integrate
 *@param[a] the k dimensional lower limits
 *@param[b] the k dimensional upper limits
 *@param[eps] the desired accuracy of the integration
 *@param[params] the parameters for the function
 *@param[errorEstimate] the error estimate of the integration function*/
double Integrate(
	int functionCode, // Identifies the function (and dimensionality k)
  	const float *a, // An array of k lower bounds
  	const float *b, // An array of k upper bounds
  	float eps,  // A target accuracy
  	const float *params, // Parameters to function
  	float *errorEstimate // Estimated error in integral
);

#endif