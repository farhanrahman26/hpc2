#ifndef KERNEL_F5_HPP
#define KERNEL_F5_HPP
#include "Kernel.hpp"
#if defined(__APPLE__) || defined(MACOSX)
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif
#include "CLError.hpp"

#define MAX_F5_BUFFER 512*512*512

class KernelF5 : public Kernel{
public:
       KernelF5(
                cl_program clProgram,
                cl_command_queue commandQueue,
                cl_context ctx,
                const float* a,
                const float* b,
                int n0,
                int n1,
                int n2,
                std::string kernelName = "F5_GPU"
        ) : Kernel(clProgram, commandQueue, ctx, kernelName, 
				a, b, 3, NULL, 0, n0, n1, n2, reductionFactor, MAX_F5_BUFFER
			) 
        {
        }

        ~KernelF5(void){
                clReleaseMemObject(d_y);
        }
};

#endif
