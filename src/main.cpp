#include "integrate.hpp"
#include "headers.hpp"
#include <iostream>
#include <vector>
#include <string>
#include <tbb/tick_count.h>
#include <assert.h>
#include <math.h>
#include "simpsons.hpp"
#define RELEASE 0

/*Test for Dumb implementations*/
void Test0(const CLInitialiser& cli)
{
	double exact=(exp(1)-1);	// Exact result
	float a[1]={0};
	float b[1]={1};
	int n;

	for(n=2;n<=512;n*=2){		
		
		KernelF0 kernelF0(
			cli.clProgram, 
			cli.commandQueue, 
			cli.ctx, 
			&a[0], 
			&b[0], 
			n
		);

		Integrator1D integrator(
			0,
			&a[0],
			&b[0],
			100.0,
			NULL,
			NULL
		);

		tbb::tick_count start = tbb::tick_count::now();		
		double res= integrator.integrate(n, kernelF0);
		tbb::tick_count end = tbb::tick_count::now();
		printf("F0, n=%d, value=%lf, error=%lg, time=%lf\n", n, res, res-exact, (end-start).seconds());
	}
}

void Test1(const CLInitialiser& cli)
{
	double exact=1.95683793560212f;
	float a[2]={0,0};
	float b[2]={1,1};
	float params[2]={0.5,0.5};
	int n;
	
	for(n=2;n<=1024;n*=2){		

		KernelF1 kernelF1(
			cli.clProgram, 
			cli.commandQueue, 
			cli.ctx, 
			&a[0], 
			&b[0], 
			&params[0], 
			n, 
			n
		);

		Integrator2D integrator(
			1, 
			&a[0], 
			&b[0], 
			100.0,
			&params[0],
			NULL
		);

		tbb::tick_count start = tbb::tick_count::now();		
		double res = integrator.integrate(n, kernelF1);
		tbb::tick_count end = tbb::tick_count::now();
		printf("F1, n=%d, value=%lf, error=%lg, time=%lf\n", n, res, res-exact, (end-start).seconds());
	}
}

void Test2(const CLInitialiser &cli)
{
	double exact=9.48557252267795;	// Correct to about 6 digits
	float a[3]={-1,-1,-1};
	float b[3]={1,1,1};
	int n;
	
	for(n=2;n<=256;n*=2){	
		KernelF2 kernelF2(
			cli.clProgram, 
			cli.commandQueue, 
			cli.ctx, 
			&a[0], 
			&b[0], 
			n, 
			n, 
			n
		);

		Integrator3D integrator(
			2,
			&a[0],
			&b[0],
			100.0,
			NULL,
			NULL
		);
		tbb::tick_count start = tbb::tick_count::now();		
		double res = integrator.integrate(n, kernelF2);
		tbb::tick_count end = tbb::tick_count::now();
		printf("F2, n=%d, value=%lf, error=%lg, time=%lf\n", n, res, res-exact, (end-start).seconds());
	}
}

void Test3(const CLInitialiser &cli)
{
	double exact=-7.18387139942142f;	// Correct to about 6 digits
	float a[3]={0,0,0};
	float b[3]={5,5,5};
	float params[1]={2};
	int n;
	
	for(n=2;n<=256;n*=2){		
		KernelF3 kernelF3(
			cli.clProgram, 
			cli.commandQueue, 
			cli.ctx, 
			&a[0], 
			&b[0],
			&params[0], 
			n, 
			n, 
			n
		);

		Integrator3D integrator(
			3,
			&a[0],
			&b[0],
			100.0,
			NULL,
			NULL
		);

		tbb::tick_count start = tbb::tick_count::now();		
		double res = integrator.integrate(n, kernelF3);
		tbb::tick_count end = tbb::tick_count::now();

		printf("F3, n=%d, value=%lf, error=%lg, time=%lf\n", n, res, res-exact, (end-start).seconds());
	}
}

void Test4(const CLInitialiser &cli)
{
	double exact=0.677779532970409f;
	float a[3]={-16,-16,-16};
	float b[3]={1,1,1};
	
	const float PI=3.1415926535897932384626433832795f;
	float params[10]={
		1.5, -0.5, -0.5,
		-0.5, 1.5, -0.5,
		-0.5, -0.5, 1.5,
		pow(2*PI,-3.0/2.0)*pow(0.5,-0.5)
	};
	int n;
	
	for(n=2;n<=512;n*=2){		
		KernelF4 kernelF4(
			cli.clProgram, 
			cli.commandQueue, 
			cli.ctx, 
			&a[0], 
			&b[0], 
			&params[0], 
			n, 
			n,
			n
		);

		Integrator3D integrator(
			4, 
			&a[0], 
			&b[0], 
			100.0,
			&params[0],
			NULL
		);
		tbb::tick_count start = tbb::tick_count::now();		
		double res = integrator.integrate(n, kernelF4);
		tbb::tick_count end = tbb::tick_count::now();

		printf("F4, n=%d, value=%lf, error=%lg, time=%lf\n", n, res, res-exact, (end-start).seconds());
	}
}

void Test5(const CLInitialiser &cli)
{
	double exact=13.4249394627056;	// Correct to about 6 digits
	float a[3]={0,0,0};
	float b[3]={3,3,3};
	#if RELEASE == 1
	float errorEstimate = 0.0f;
	double res = Integrate(
		5, // Identifies the function (and dimensionality k)
	  	a, // An array of k lower bounds
	  	b, // An array of k upper bounds
	  	0.0000005,  // A target accuracy
	  	NULL, // Parameters to function
	  	&errorEstimate // Estimated error in integral
	);

	printf("F5 Simpsons, value=%lf, error=%lg, errorEstimate=%lf\n", res, res-exact, errorEstimate);

	#else	
	int n;
	
	for(n=2;n<=512;n*=2){
		KernelF5 kernelF5(
			cli.clProgram, 
			cli.commandQueue, 
			cli.ctx, 
			&a[0], 
			&b[0], 
			n, 
			n,
			n
		);

		Integrator3D integrator(
			5, 
			&a[0], 
			&b[0], 
			100.0,
			NULL,
			NULL
		);

		tbb::tick_count start = tbb::tick_count::now();		
		double res = integrator.integrate(n, kernelF5);
		tbb::tick_count end = tbb::tick_count::now();

		printf("F5, n=%d, value=%lf, error=%lg, time=%lf\n", n, res, res-exact, (end-start).seconds());
	}
	#endif
}

void Test6(const CLInitialiser &cli)
{
	double exact=   2.261955088165;
	float a[3]={-4,-4,-4};
	float b[3]={4,4,4};
	float params[2]={3,0.01};
	int n;
	
	for(n=2;n<=512;n*=2){		
		KernelF6 kernelF6(
			cli.clProgram, 
			cli.commandQueue, 
			cli.ctx, 
			&a[0], 
			&b[0], 
			&params[0], 
			n, 
			n,
			n
		);

		Integrator3D integrator(
			6, 
			&a[0], 
			&b[0], 
			100.0,
			&params[0],
			NULL
		);

		tbb::tick_count start = tbb::tick_count::now();		
		double res = integrator.integrate(n, kernelF6);
		tbb::tick_count end = tbb::tick_count::now();
		printf("F6, n=%d, value=%lf, error=%lg, time=%lf\n", n, res, res-exact, (end-start).seconds());
	}
}


/*Test for simpson's implementation*/
void Test0Simp(const CLInitialiser &cli){
	double exact=(exp(1)-1);	// Exact result
	float a[1]={0};
	float b[1]={1};
	#if RELEASE == 1
	float errorEstimate = 0.0f;
	double res = Integrate(
		0, // Identifies the function (and dimensionality k)
	  	a, // An array of k lower bounds
	  	b, // An array of k upper bounds
	  	0.0000005,  // A target accuracy
	  	NULL, // Parameters to function
	  	&errorEstimate // Estimated error in integral
	);

	printf("F0 Simpsons, value=%lf, error=%lg, errorEstimate=%lf\n", res, res-exact, errorEstimate);

	#else
	int n;
	for(n=2;n<=1024;n*=2){		
		
		KernelF0 kernelF0(
			cli.clProgram, 
			cli.commandQueue, 
			cli.ctx, 
			&a[0], 
			&b[0], 
			n,
			"F0_GPU_SIMP"
		);

		Integrator1DSimp integrator(
			0,
			&a[0],
			&b[0],
			100.0,
			NULL,
			NULL
		);

		tbb::tick_count start = tbb::tick_count::now();		
		double res= integrator.integrate(n, kernelF0);
		tbb::tick_count end = tbb::tick_count::now();
		printf("F0 Simpsons, n=%d, value=%lf, error=%lg, time=%lf\n", n, res, res-exact, (end-start).seconds());
	}
	#endif
}

void Test1Simp(const CLInitialiser &cli){
	double exact=1.95683793560212f;
	float a[2]={0,0};
	float b[2]={1,1};
	float params[2]={0.5,0.5};
	
	#if RELEASE == 1
	float errorEstimate = 0.0f;
	double res = Integrate(
		1, // Identifies the function (and dimensionality k)
	  	a, // An array of k lower bounds
	  	b, // An array of k upper bounds
	  	0.0000005,  // A target accuracy
	  	params, // Parameters to function
	  	&errorEstimate // Estimated error in integral
	);

	printf("F1 Simpsons, value=%lf, error=%lg, errorEstimate=%lf\n", res, res-exact, errorEstimate);

	#else	
	int n;	
	for(n=2;n<=1024;n*=2){		

		KernelF1 kernelF1(
			cli.clProgram, 
			cli.commandQueue, 
			cli.ctx, 
			&a[0], 
			&b[0], 
			&params[0], 
			n, 
			n,
			"F1_GPU_SIMP"
		);

		Integrator2DSimp integrator(
			1, 
			&a[0], 
			&b[0], 
			100.0,
			&params[0],
			NULL
		);

		tbb::tick_count start = tbb::tick_count::now();		
		double res = integrator.integrate(n, kernelF1);
		tbb::tick_count end = tbb::tick_count::now();
		printf("F1 Simpsons, n=%d, value=%lf, error=%lg, time=%lf\n", n, res, res-exact, (end-start).seconds());
	}
	#endif
}

void Test2Simp(const CLInitialiser &cli)
{
	double exact=9.48557252267795;	// Correct to about 6 digits
	float a[3]={-1,-1,-1};
	float b[3]={1,1,1};
	
	#if RELEASE == 1
	float errorEstimate = 0.0f;
	double res = Integrate(
		2, // Identifies the function (and dimensionality k)
	  	a, // An array of k lower bounds
	  	b, // An array of k upper bounds
	  	0.0000005,  // A target accuracy
	  	NULL, // Parameters to function
	  	&errorEstimate // Estimated error in integral
	);

	printf("F2 Simpsons, value=%lf, error=%lg, errorEstimate=%lf\n", res, res-exact, errorEstimate);

	#else
	int n;	


	for(n=2;n<=512;n*=2){	
		KernelF2 kernelF2(
			cli.clProgram, 
			cli.commandQueue, 
			cli.ctx, 
			&a[0], 
			&b[0], 
			n, 
			n, 
			n,
			"F2_GPU_SIMP"
		);

		Integrator3DSimp integrator(
			2,
			&a[0],
			&b[0],
			100.0,
			NULL,
			NULL
		);

		tbb::tick_count start = tbb::tick_count::now();		
		double res = integrator.integrate(n, kernelF2);
		tbb::tick_count end = tbb::tick_count::now();
		printf("F2 Simpson, n=%d, value=%lf, error=%lg, time=%lf\n", n, res, res-exact, (end-start).seconds());
	}
	#endif
}

void Test3Simp(const CLInitialiser &cli)
{
	double exact=-7.18387139942142f;	// Correct to about 6 digits
	float a[3]={0,0,0};
	float b[3]={5,5,5};
	float params[1]={2};
	#if RELEASE == 1
	float errorEstimate = 0.0f;
	double res = Integrate(
		3, // Identifies the function (and dimensionality k)
	  	a, // An array of k lower bounds
	  	b, // An array of k upper bounds
	  	0.0000005,  // A target accuracy
	  	params, // Parameters to function
	  	&errorEstimate // Estimated error in integral
	);

	printf("F3 Simpsons, value=%lf, error=%lg, errorEstimate=%lf\n", res, res-exact, errorEstimate);

	#else	
	int n;
	
	for(n=2;n<=512;n*=2){		
		KernelF3 kernelF3(
			cli.clProgram, 
			cli.commandQueue, 
			cli.ctx, 
			&a[0], 
			&b[0],
			&params[0], 
			n, 
			n, 
			n,
			"F3_GPU_SIMP"
		);

		Integrator3DSimp integrator(
			3,
			&a[0],
			&b[0],
			100.0,
			NULL,
			NULL
		);

		tbb::tick_count start = tbb::tick_count::now();		
		double res = integrator.integrate(n, kernelF3);
		tbb::tick_count end = tbb::tick_count::now();

		printf("F3 Simpson, n=%d, value=%lf, error=%lg, time=%lf\n", n, res, res-exact, (end-start).seconds());
	}
	#endif
}

void Test4Simp(const CLInitialiser &cli)
{
	double exact=0.677779532970409f;
	float a[3]={-16,-16,-16};
	float b[3]={1,1,1};
	
	const float PI=3.1415926535897932384626433832795f;
	float params[10]={
		1.5, -0.5, -0.5,
		-0.5, 1.5, -0.5,
		-0.5, -0.5, 1.5,
		pow(2*PI,-3.0/2.0)*pow(0.5,-0.5)
	};
	#if RELEASE == 1
	float errorEstimate = 0.0f;
	double res = Integrate(
		4, // Identifies the function (and dimensionality k)
	  	a, // An array of k lower bounds
	  	b, // An array of k upper bounds
	  	0.0000005,  // A target accuracy
	  	params, // Parameters to function
	  	&errorEstimate // Estimated error in integral
	);

	printf("F4 Simpsons, value=%lf, error=%lg, errorEstimate=%lf\n", res, res-exact, errorEstimate);

	#else	
	int n;
	
	for(n=2;n<=512;n*=2){		
		KernelF4 kernelF4(
			cli.clProgram, 
			cli.commandQueue, 
			cli.ctx, 
			&a[0], 
			&b[0], 
			&params[0], 
			n, 
			n,
			n,
			"F4_GPU_SIMP"
		);

		Integrator3DSimp integrator(
			4, 
			&a[0], 
			&b[0], 
			100.0,
			&params[0],
			NULL
		);
		tbb::tick_count start = tbb::tick_count::now();		
		double res = integrator.integrate(n, kernelF4);
		tbb::tick_count end = tbb::tick_count::now();

		printf("F4 Simpson, n=%d, value=%lf, error=%lg, time=%lf\n", n, res, res-exact, (end-start).seconds());
	}
	#endif
}

void Test5Simp(const CLInitialiser &cli)
{
	double exact=13.4249394627056;	// Correct to about 6 digits
	float a[3]={0,0,0};
	float b[3]={3,3,3};
	int n;
	
	for(n=2;n<=512;n*=2){
		KernelF5 kernelF5(
			cli.clProgram, 
			cli.commandQueue, 
			cli.ctx, 
			&a[0], 
			&b[0], 
			n, 
			n,
			n,
			"F5_GPU_SIMP"
		);

		Integrator3DSimp integrator(
			5, 
			&a[0], 
			&b[0], 
			100.0,
			NULL,
			NULL
		);

		tbb::tick_count start = tbb::tick_count::now();		
		double res = integrator.integrate(n, kernelF5);
		tbb::tick_count end = tbb::tick_count::now();
		printf("F5 Simpson, n=%d, value=%lf, error=%lg, time=%lf\n", n, res, res-exact, (end-start).seconds());
	}
}

void Test6Simp(const CLInitialiser &cli)
{
	double exact=   2.261955088165;
	float a[3]={-4,-4,-4};
	float b[3]={4,4,4};
	float params[2]={3,0.01};
	#if RELEASE == 1
	float errorEstimate = 0.0f;
	double res = Integrate(
		6, // Identifies the function (and dimensionality k)
	  	a, // An array of k lower bounds
	  	b, // An array of k upper bounds
	  	0.0000005,  // A target accuracy
	  	params, // Parameters to function
	  	&errorEstimate // Estimated error in integral
	);

	printf("F6 Simpsons, value=%lf, error=%lg, errorEstimate=%lf\n", res, res-exact, errorEstimate);

	#else	
	int n;
	
	for(n=2;n<=512;n*=2){		
		KernelF6 kernelF6(
			cli.clProgram, 
			cli.commandQueue, 
			cli.ctx, 
			&a[0], 
			&b[0], 
			&params[0], 
			n, 
			n,
			n,
			"F6_GPU_SIMP"
		);

		Integrator3DSimp integrator(
			6, 
			&a[0], 
			&b[0], 
			100.0,
			&params[0],
			NULL
		);

		tbb::tick_count start = tbb::tick_count::now();		
		double res = integrator.integrate(n, kernelF6);
		tbb::tick_count end = tbb::tick_count::now();
		printf("F6 Simpson, n=%d, value=%lf, error=%lg, time=%lf\n", n, res, res-exact, (end-start).seconds());
	}
	#endif
}

int main(int argc, char * argv[]){
	std::vector<std::string> programSources;
	programSources.push_back("functions.cl");

	/*Initialise everything related to OpenCL*/
	CLInitialiser clInitialiser(programSources);

	//Test0(clInitialiser);	
	Test0Simp(clInitialiser);
	//Test1(clInitialiser);
 	Test1Simp(clInitialiser);
// 	//Test2(clInitialiser);
 	Test2Simp(clInitialiser);
// 	//Test3(clInitialiser);
 	Test3Simp(clInitialiser);
// 	//Test4(clInitialiser);
 	Test4Simp(clInitialiser);
 	Test5(clInitialiser);
 //	Test5Simp(clInitialiser);
// 	//Test6(clInitialiser);
 	Test6Simp(clInitialiser);

	return 0;
}
