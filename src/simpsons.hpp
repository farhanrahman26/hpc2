#ifndef SIMPSON_HPP
#define SIMPSON_HPP
/*=====================Test code for simpsons==================*/

double Integrator1DSimpsons(float *a, float *b, int n);

void TestSimpsonF0(void);

double F1Simp(float x, float y, float *params);

double Integrator2DSimpsons(float *a, float *b, int n, float *params);

void TestSimpsonF1(void);


/*==============3D functions===================*/
float F2_S(const float x, const float y, const float z, const float *params);

float F3_S(const float x, const float y, const float z, const float *params);

float F4_S(const float x, const float y, const float z, const float *params);

float F5_S(const float x, const float y, const float z, const float *params);

float F6_S(const float x, const float y, const float z, const float *params);

double Integrator3DSimpsons(float *a, float *b, int n, float *params, int functionCode);

void TestSimpsonF2();

void TestSimpsonF3();

void TestSimpsonF4();

void TestSimpsonF5();

void TestSimpsonF6();

#endif
